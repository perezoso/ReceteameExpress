package org.escoladeltreball.iam46710930.testfragment.pojos;

import java.io.Serializable;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class Ingredient implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id_ingredient;
    private Integer id_recepta;
    private String nom_ingredient;
    private Integer quantitat;
    private String tipus_quantitat;

    public Ingredient() {
    }

    public Ingredient(String nom_ingredient, Integer quantitat, String tipus_quantitat) {
        this.nom_ingredient = nom_ingredient;
        this.quantitat = quantitat;
        this.tipus_quantitat = tipus_quantitat;
    }

    public Ingredient(Integer id_ingredient, Integer id_recepta, String nom_ingredient, Integer quantitat, String tipus_quantitat) {
        this.id_ingredient = id_ingredient;
        this.id_recepta = id_recepta;
        this.nom_ingredient = nom_ingredient;
        this.quantitat = quantitat;
        this.tipus_quantitat = tipus_quantitat;
    }


    public Integer getId_ingredient() {
        return id_ingredient;
    }


    public void setId_ingredient(Integer id_ingredient) {
        this.id_ingredient = id_ingredient;
    }


    public Integer getId_recepta() {
        return id_recepta;
    }


    public void setId_recepta(Integer id_recepta) {
        this.id_recepta = id_recepta;
    }


    public String getNom_ingredient() {
        return nom_ingredient;
    }


    public void setNom_ingredient(String nom_ingredient) {
        this.nom_ingredient = nom_ingredient;
    }


    public Integer getQuantitat() {
        return quantitat;
    }


    public void setQuantitat(Integer quantitat) {
        this.quantitat = quantitat;
    }


    public String getTipus_quantitat() {
        return tipus_quantitat;
    }


    public void setTipus_quantitat(String tipus_quantitat) {
        this.tipus_quantitat = tipus_quantitat;
    }


}
