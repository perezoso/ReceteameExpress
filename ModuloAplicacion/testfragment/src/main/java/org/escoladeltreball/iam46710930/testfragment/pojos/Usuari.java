package org.escoladeltreball.iam46710930.testfragment.pojos;

import java.io.Serializable;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class Usuari implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id_usuari;
    private String nom_usuari;
    private String contrasenya_usuari;
    private String mail_usuari;
    private String token;
    private Respuesta respuesta;
    private Integer cantidadReceptas;
    private Integer cantidadSeguidores;
    private boolean isFollow;
    private Integer cantidadLikes;

    public Usuari() {
    }

    public Usuari(String nom_usuari, String mail_usuari, String contrasenya_usuari, String token, Respuesta respuesta) {
        this.nom_usuari = nom_usuari;
        this.mail_usuari = mail_usuari;
        this.contrasenya_usuari = contrasenya_usuari;
        this.token = token;
        this.respuesta = respuesta;
    }

    public Usuari(Integer id_usuari, String nom_usuari, String contrasenya_usuari, String mail_usuari, String token) {
        this.id_usuari = id_usuari;
        this.nom_usuari = nom_usuari;
        this.contrasenya_usuari = contrasenya_usuari;
        this.mail_usuari = mail_usuari;
        this.token = token;
    }

    public Integer getId_usuari() {
        return id_usuari;
    }

    public void setId_usuari(Integer id_usuari) {
        this.id_usuari = id_usuari;
    }

    public String getNom_usuari() {
        return nom_usuari;
    }

    public void setNom_usuari(String nom_usuari) {
        this.nom_usuari = nom_usuari;
    }

    public String getContrasenya_usuari() {
        return contrasenya_usuari;
    }

    public void setContrasenya_usuari(String contrasenya_usuari) {
        this.contrasenya_usuari = contrasenya_usuari;
    }

    public String getMail_usuari() {
        return mail_usuari;
    }

    public void setMail_usuari(String mail_usuari) {
        this.mail_usuari = mail_usuari;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Respuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    @Override
    public String toString() {
        return "Usuari [id_usuari=" + id_usuari + ", nom_usuari=" + nom_usuari + ", mail_usuari=" + mail_usuari + ", token=" + token + ", respuesta=" + respuesta
                + "]";
    }

    public Integer getCantidadReceptas() {
        return cantidadReceptas;
    }

    public void setCantidadReceptas(Integer cantidadReceptas) {
        this.cantidadReceptas = cantidadReceptas;
    }

    public Integer getCantidadSeguidores() {
        return cantidadSeguidores;
    }

    public void setCantidadSeguidores(Integer cantidadSeguidores) {
        this.cantidadSeguidores = cantidadSeguidores;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setIsFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    public Integer getCantidadLikes() {
        return cantidadLikes;
    }

    public void setCantidadLikes(Integer cantidadLikes) {
        this.cantidadLikes = cantidadLikes;
    }
}
