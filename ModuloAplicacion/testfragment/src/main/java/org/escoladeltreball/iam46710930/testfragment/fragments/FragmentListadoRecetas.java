package org.escoladeltreball.iam46710930.testfragment.fragments;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.activities.MainActivity;
import org.escoladeltreball.iam46710930.testfragment.adapters.AdapterListadoRecetas;
import org.escoladeltreball.iam46710930.testfragment.pojos.Filtres;
import org.escoladeltreball.iam46710930.testfragment.pojos.Recepta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;
import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class FragmentListadoRecetas extends Fragment implements View.OnClickListener {

    ConectionRetrofit conectionRetrofit;
    RecyclerView recyclerView;
    AdapterListadoRecetas adapter_listado_recetas;
    SearchView search;
    ArrayList<Recepta> receptes;
    View rootView;
    static String filtro;
    MainActivity mainActivity;
    static SharedPreferences prefe;
    SearchView.OnQueryTextListener listener;

    public static FragmentListadoRecetas newInstance(String filtro_var, SharedPreferences sharedPreferences) {
        FragmentListadoRecetas fragment_listado_recetas = new FragmentListadoRecetas();
        filtro = filtro_var;
        prefe = sharedPreferences;
        return fragment_listado_recetas;

    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.listado_recetas, container, false);

        conectionRetrofit = new ConectionRetrofit();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.list_recetas);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);

        selectFiltres(conectionRetrofit.getService(), filtro);

        mainActivity = (MainActivity) getActivity();

        inicializa_search();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * Inicializa SearchView
     */
    public void inicializa_search() {
        listener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query) {
                query = query.toLowerCase();

                final ArrayList<Recepta> filteredList = new ArrayList<>();

                for (int i = 0; i < receptes.size(); i++) {

                    final String text = receptes.get(i).getTitol_recepta().toLowerCase();
                    if (text.contains(query)) {

                        filteredList.add(receptes.get(i));
                    }
                }
                adapter_listado_recetas = new AdapterListadoRecetas(filteredList, getContext(), FragmentListadoRecetas.this);
                recyclerView.setAdapter(adapter_listado_recetas);
                adapter_listado_recetas.notifyDataSetChanged();
                return true;

            }

            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        };
    }

    /**
     * Solicita listado al servidor de recetas con una palabra de filtro
     *
     * @param service
     * @param filtro
     */
    public void selectFiltres(Service service, String filtro) {

        Gson gson = new Gson();
        Usuari usuario = null;
        if (filtro.equals("Mis likes")) {
            usuario = new Usuari();
            usuario.setId_usuari(Integer.parseInt(prefe.getString("id_usuario", "")));
        }
        String jsonUsuario = gson.toJson(usuario);
        RequestBody requestUsuario = RequestBody.create(MediaType.parse("text/plain"), jsonUsuario);

        Filtres filtres = new Filtres(filtro);
        String jsonFiltro = gson.toJson(filtres);
        RequestBody requestFiltro = RequestBody.create(MediaType.parse("text/plain"), jsonFiltro);

        Call<ArrayList<Recepta>> call = service.filtros(requestFiltro, requestUsuario);
        call.enqueue(new Callback<ArrayList<Recepta>>() {
            @Override
            public void onResponse(Response<ArrayList<Recepta>> response) {
                receptes = response.body();
                if (rootView != null) {
                    recarga();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                Log.e("MainActivity", "onFailure", t);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void recarga() {
        adapter_listado_recetas = new AdapterListadoRecetas(receptes, getContext(), this);
        recyclerView.setAdapter(adapter_listado_recetas);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        search = (SearchView) rootView.findViewById(R.id.buscador_listas);
        search.setOnQueryTextListener(listener);
    }

    @Override
    public void onClick(View v) {
        int id_recepta = adapter_listado_recetas.getArrayRecetas().get((Integer) v.getTag()).getId_recepta();
        Fragment newFragment = FragmentVisualizacionReceta.newInstance(id_recepta, false, prefe);
        mainActivity.cambia_fragment(newFragment);
    }

    @Override
    public void onDetach() {
        mainActivity = null;
        rootView = null;
        super.onDetach();
    }
}
