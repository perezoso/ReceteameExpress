package org.escoladeltreball.iam46710930.testfragment.fragments;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.activities.MainActivity;
import org.escoladeltreball.iam46710930.testfragment.adapters.AdapterListadoUsuarios;
import org.escoladeltreball.iam46710930.testfragment.pojos.Action;
import org.escoladeltreball.iam46710930.testfragment.pojos.Respuesta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Seguidor;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;
import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class FragmentUsuariosSeguidos extends Fragment implements View.OnClickListener {

    ConectionRetrofit conectionRetrofit;
    RecyclerView recyclerView;
    AdapterListadoUsuarios adapter_listado_usuarios;
    SearchView search;
    ArrayList<Usuari> usuarios;
    View rootView;
    MainActivity mainActivity;
    static SharedPreferences prefe;
    Respuesta respuesta;
    Service service;
    Usuari usuarioLista;
    FloatingActionButton add_usuario;
    static boolean busca_usuario;
    SearchView.OnQueryTextListener listener;
    int posicionUsuario;
    boolean primeraCarga = false;


    public static FragmentUsuariosSeguidos newInstance(SharedPreferences sharedPreferences, boolean buscaUsuario) {
        FragmentUsuariosSeguidos fragment_usuarios_seguidos = new FragmentUsuariosSeguidos();
        prefe = sharedPreferences;
        busca_usuario = buscaUsuario;
        return fragment_usuarios_seguidos;

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.listado_seguidos, container, false);
        conectionRetrofit = new ConectionRetrofit();
        service = conectionRetrofit.getService();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.list_seguidos);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);

        selectUsuarios();

        mainActivity = (MainActivity) getActivity();
        add_usuario = (FloatingActionButton) rootView.findViewById(R.id.add_usuario);
        if (!busca_usuario) {
            add_usuario.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Fragment new_fragment = FragmentUsuariosSeguidos.newInstance(prefe, true);
                    mainActivity.cambia_fragment(new_fragment);
                }
            });
        } else {
            add_usuario.setVisibility(View.GONE);
        }

        inicializacion_search();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void inicializacion_search() {
        listener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query) {
                query = query.toLowerCase();

                final ArrayList<Usuari> filteredList = new ArrayList<>();

                for (int i = 0; i < usuarios.size(); i++) {

                    final String text = usuarios.get(i).getNom_usuari().toLowerCase();
                    if (text.contains(query)) {

                        filteredList.add(usuarios.get(i));
                    }
                }
                adapter_listado_usuarios = new AdapterListadoUsuarios(filteredList, getContext(), FragmentUsuariosSeguidos.this);
                recyclerView.setAdapter(adapter_listado_usuarios);
                adapter_listado_usuarios.notifyDataSetChanged();
                return true;

            }

            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        };
    }

    /**
     * Solicita listado usuarios al servidor
     */
    public void selectUsuarios() {

        Gson gson = new Gson();
        Seguidor seguidor = new Seguidor();
        seguidor.setId_usuari(Integer.parseInt(prefe.getString("id_usuario", "")));
        String jsonUsuario = gson.toJson(seguidor);
        RequestBody requestUsuario = RequestBody.create(MediaType.parse("text/plain"), jsonUsuario);
        Action action;
        if (!busca_usuario) {
            action = new Action("selectUsuariosSeguidos");
        } else {
            action = new Action("selectAllUsuarios");
        }
        String jsonAction = gson.toJson(action);
        RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

        Call<ArrayList<Usuari>> call = service.usuarios_seguidos(requestAction, requestUsuario);
        call.enqueue(new Callback<ArrayList<Usuari>>() {
            @Override
            public void onResponse(Response<ArrayList<Usuari>> response) {
                usuarios = response.body();
                if (rootView != null) {
                    if (!primeraCarga) {
                        recarga(true);
                    } else {
                        recarga(false);
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                Log.e("MainActivity", "onFailure", t);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void recarga(boolean cargaInicial) {
        if (cargaInicial) {
            adapter_listado_usuarios = new AdapterListadoUsuarios(usuarios, getContext(), this);
        }else{
            adapter_listado_usuarios = new AdapterListadoUsuarios(adapter_listado_usuarios.getArrayUsuarios(), getContext(), this);
        }
        recyclerView.setAdapter(adapter_listado_usuarios);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        search = (SearchView) rootView.findViewById(R.id.buscador_listas);
        search.setOnQueryTextListener(listener);
    }

    @Override
    public void onClick(View v) {
        posicionUsuario = Integer.parseInt(v.getTag().toString().split("-")[0]);
        String accion = v.getTag().toString().split("-")[1];
        usuarioLista = adapter_listado_usuarios.getArrayUsuarios().get(posicionUsuario);
        if (accion.equals("star")) {
            if (!busca_usuario) {
                verifica_eliminacion(v);
            } else {
                if (!((CheckBox) v).isChecked()) {
                    verifica_eliminacion(v);
                } else {
                    modifica_seguidor(usuarioLista.getId_usuari(), ((CheckBox) v).isChecked());
                }
            }
        } else if (accion.equals("ver_recetas")) {
            int id_usuario = usuarioLista.getId_usuari();
            Fragment newFragment = FragmentMisRecetas.newInstance(prefe, id_usuario);
            mainActivity.cambia_fragment(newFragment);
        }
    }

    /**
     * Solicita modifiación al servidor del estado de seguimiento a un usuario
     *
     * @param id_usuario
     * @param isChecked
     */
    public void modifica_seguidor(int id_usuario, final boolean isChecked) {
        Gson gson = new Gson();
        Seguidor seguidor = new Seguidor(Integer.parseInt(prefe.getString("id_usuario", "")), id_usuario);
        String jsonSeguidor = gson.toJson(seguidor);
        RequestBody requestSeguidor = RequestBody.create(MediaType.parse("text/plain"), jsonSeguidor);
        Action action;
        if (!isChecked) {
            action = new Action("delete");
        } else {
            action = new Action("insert");
        }
        String jsonAction = gson.toJson(action);
        RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

        Call<Respuesta> call = service.seguir_usuario(requestAction, requestSeguidor);
        call.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Response<Respuesta> response) {
                respuesta = response.body();
                if (respuesta.isRespuesta() && rootView != null) {
                    if (!busca_usuario) {
                        usuarios.remove(usuarioLista);
                        adapter_listado_usuarios.getArrayUsuarios().remove(usuarioLista);
                        recarga(false);
                    }else{
                        int pos = usuarios.indexOf(usuarioLista);
                        if (!isChecked) {
                            usuarioLista.setIsFollow(false);
                            usuarios.set(pos, usuarioLista);
                            adapter_listado_usuarios.getArrayUsuarios().get(posicionUsuario).setIsFollow(false);
                            recarga(false);
                        }else{
                            usuarioLista.setIsFollow(true);
                            usuarios.set(pos, usuarioLista);
                            adapter_listado_usuarios.getArrayUsuarios().get(posicionUsuario).setIsFollow(true);
                            recarga(false);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                Log.e("MainActivity", "onFailure", t);
            }
        });
    }

    /**
     * Verifica con AlertDialog la finalización de seguimiento a un usuario
     *
     * @param v
     */
    public void verifica_eliminacion(final View v) {
        AlertDialog.Builder alert = new AlertDialog.Builder(mainActivity);
        alert.setTitle("Eliminar usuarioLista");
        alert.setMessage("Estas seguro de dejar de seguir a este usuario?");
        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                modifica_seguidor(usuarioLista.getId_usuari(), ((CheckBox) v).isChecked());

            }
        });
        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((CheckBox) v).setChecked(true);
            }
        });

        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((CheckBox) v).setChecked(true);
            }
        });

        alert.show();


    }

    @Override
    public void onDetach() {
        mainActivity = null;
        rootView = null;
        super.onDetach();
    }
}
