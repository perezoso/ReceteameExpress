package org.escoladeltreball.iam46710930.testfragment.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

import org.escoladeltreball.iam46710930.testfragment.pojos.InfoPrincipalReceta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Ingredient;
import org.escoladeltreball.iam46710930.testfragment.pojos.LineaIngredienteInsercion;
import org.escoladeltreball.iam46710930.testfragment.pojos.LineaPasoInsercion;
import org.escoladeltreball.iam46710930.testfragment.pojos.Pas;
import org.escoladeltreball.iam46710930.testfragment.pojos.Recepta;

import java.util.ArrayList;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class EditReceta {

    Recepta recepta;
    InfoPrincipalReceta info_principal_receta;
    ArrayList<LineaIngredienteInsercion> lista_lineas_ingrdientes;
    ArrayList<LineaPasoInsercion> lista_lineas_pasos;
    ArrayList<Boolean> check_imagenes_insertadas;
    ArrayList<String> array_categoria_menu;
    ArrayList<String> array_tipo_cantidad;

    public EditReceta(Recepta recepta, InfoPrincipalReceta info_principal_receta, ArrayList<LineaIngredienteInsercion> lista_lineas_ingrdientes,
                      ArrayList<LineaPasoInsercion> lista_lineas_pasos, ArrayList<Boolean> check_imagenes_insertadas,
                      ArrayList<String> array_categoria_menu, ArrayList<String> array_tipo_cantidad) {
        this.recepta = recepta;
        this.info_principal_receta = info_principal_receta;
        this.lista_lineas_ingrdientes = lista_lineas_ingrdientes;
        this.lista_lineas_pasos = lista_lineas_pasos;
        this.check_imagenes_insertadas = check_imagenes_insertadas;
        this.array_categoria_menu = array_categoria_menu;
        this.array_tipo_cantidad = array_tipo_cantidad;
        carga_receta_propia();
    }

    /**
     * Lanza métodos que realizarán la carga de la información
     */
    public void carga_receta_propia() {
        carga_info_principal();
        carga_ingredientes();
        carga_pasos();
    }

    /**
     * Realiza carga información de la parte principal de la receta
     */
    public void carga_info_principal() {
        info_principal_receta.getTitolRecepta().setText(recepta.getTitol_recepta());
        info_principal_receta.getCategoriaMenu().setSelection(array_categoria_menu.indexOf(recepta.getCategoria_menu()));
        info_principal_receta.getNumComensales().setText(recepta.getNumero_comensals().toString());
        info_principal_receta.getNumVisibIngredientes().setText(recepta.getListIngredients().size() + "/6");
        info_principal_receta.getNumVisibPasos().setText(recepta.getListPassos().size() + "/3");
        info_principal_receta.getImagenReceta().setImageBitmap(BitmapFactory.decodeByteArray(recepta.getImatge_recepta(), 0, recepta.getImatge_recepta().length));
    }

    /**
     * Realizar la carga de la información de los ingredientes
     */
    public void carga_ingredientes() {
        ArrayList<Ingredient> lista_ingredientes_receta = recepta.getListIngredients();
        Ingredient ingrediente_actual = lista_ingredientes_receta.get(0);
        LineaIngredienteInsercion linea_ingrediente_actual = lista_lineas_ingrdientes.get(0);
        linea_ingrediente_actual.getNombre_ingrediente().setText(ingrediente_actual.getNom_ingredient());
        linea_ingrediente_actual.getCantidad_ingrediente().setText(ingrediente_actual.getQuantitat().toString());
        linea_ingrediente_actual.getTipo_cantidad().setSelection(array_tipo_cantidad.indexOf(ingrediente_actual.getTipus_quantitat()));

        for (int i = 1; i < lista_ingredientes_receta.size(); i++) {
            linea_ingrediente_actual = lista_lineas_ingrdientes.get(i);
            linea_ingrediente_actual.getNombre_ingrediente().setVisibility(View.VISIBLE);
            linea_ingrediente_actual.getCantidad_ingrediente().setVisibility(View.VISIBLE);
            linea_ingrediente_actual.getTipo_cantidad().setVisibility(View.VISIBLE);

            ingrediente_actual = lista_ingredientes_receta.get(i);

            linea_ingrediente_actual.getNombre_ingrediente().setText(ingrediente_actual.getNom_ingredient());
            linea_ingrediente_actual.getCantidad_ingrediente().setText(ingrediente_actual.getQuantitat().toString());
            linea_ingrediente_actual.getTipo_cantidad().setSelection(array_tipo_cantidad.indexOf(ingrediente_actual.getTipus_quantitat()));
        }
    }

    /**
     * Realiza la carga de la información de los pasos
     */
    public void carga_pasos() {

        ArrayList<Pas> lista_pasos_receta = recepta.getListPassos();
        Pas paso_actual = lista_pasos_receta.get(0);
        LineaPasoInsercion linea_paso_actual = lista_lineas_pasos.get(0);
        linea_paso_actual.getDescripcion_paso().setText(paso_actual.getDescripcio_pas());
        linea_paso_actual.getImagen_paso().setImageBitmap(BitmapFactory.decodeByteArray(paso_actual.getImatge_pas(), 0, paso_actual.getImatge_pas().length));
        for (int i = 1; i < lista_pasos_receta.size(); i++) {
            linea_paso_actual = lista_lineas_pasos.get(i);
            linea_paso_actual.getDescripcion_paso().setVisibility(View.VISIBLE);
            linea_paso_actual.getImagen_paso().setVisibility(View.VISIBLE);
            linea_paso_actual.getLinea_separacion().setVisibility(View.VISIBLE);

            paso_actual = lista_pasos_receta.get(i);
            linea_paso_actual.getDescripcion_paso().setText(paso_actual.getDescripcio_pas());
            linea_paso_actual.getImagen_paso().setImageBitmap(BitmapFactory.decodeByteArray(paso_actual.getImatge_pas(), 0, paso_actual.getImatge_pas().length));
        }
    }

    /**
     * Rellena lista boolean para control de imagenes recibidas
     *
     * @return
     */
    public ArrayList<Boolean> check_imagenes() {
        for (int i = 0; i < recepta.getListPassos().size() + 1; i++) {
            check_imagenes_insertadas.set(i, true);
        }
        return check_imagenes_insertadas;
    }

    /**
     * Método convert byte [] to bitmap
     *
     * @param bytes
     * @return
     */
    public Bitmap bytes_to_bitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}
