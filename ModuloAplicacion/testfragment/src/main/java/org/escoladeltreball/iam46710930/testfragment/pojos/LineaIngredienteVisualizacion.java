package org.escoladeltreball.iam46710930.testfragment.pojos;

import android.widget.TextView;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class LineaIngredienteVisualizacion {

    private TextView nombre_ingrediente;
    private TextView cantidad;
    private TextView tipo_cantidad;

    public LineaIngredienteVisualizacion(TextView nombre_ingrediente, TextView cantidad, TextView tipo_cantidad) {
        this.nombre_ingrediente = nombre_ingrediente;
        this.cantidad = cantidad;
        this.tipo_cantidad = tipo_cantidad;
    }

    public TextView getNombre_ingrediente() {
        return nombre_ingrediente;
    }

    public void setNombre_ingrediente(TextView nombre_ingrediente) {
        this.nombre_ingrediente = nombre_ingrediente;
    }

    public TextView getCantidad() {
        return cantidad;
    }

    public void setCantidad(TextView cantidad) {
        this.cantidad = cantidad;
    }

    public TextView getTipo_cantidad() {
        return tipo_cantidad;
    }

    public void setTipo_cantidad(TextView tipo_cantidad) {
        this.tipo_cantidad = tipo_cantidad;
    }
}
