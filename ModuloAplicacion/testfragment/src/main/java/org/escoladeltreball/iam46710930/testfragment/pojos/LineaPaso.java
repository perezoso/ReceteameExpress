package org.escoladeltreball.iam46710930.testfragment.pojos;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class LineaPaso {

    private EditText descripcion_paso;
    private ImageView imagen_paso;
    private View linea_separacion;

    public LineaPaso(EditText descripcion_paso, ImageView imagen_paso, View linea_separacion) {
        this.descripcion_paso = descripcion_paso;
        this.imagen_paso = imagen_paso;
        this.linea_separacion = linea_separacion;
    }

    public LineaPaso(EditText descripcion_paso, ImageView imagen_paso) {
        this.descripcion_paso = descripcion_paso;
        this.imagen_paso = imagen_paso;
    }

    public EditText getDescripcion_paso() {
        return descripcion_paso;
    }

    public void setDescripcion_paso(EditText descripcion_paso) {
        this.descripcion_paso = descripcion_paso;
    }

    public ImageView getImagen_paso() {
        return imagen_paso;
    }

    public void setImagen_paso(ImageView imagen_paso) {
        this.imagen_paso = imagen_paso;
    }

    public View getLinea_separacion() {
        return linea_separacion;
    }

    public void setLinea_separacion(View linea_separacion) {
        this.linea_separacion = linea_separacion;
    }
}
