package org.escoladeltreball.iam46710930.testfragment.retrofit;

import org.escoladeltreball.iam46710930.testfragment.pojos.Recepta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Respuesta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public interface Service {

    @Multipart
    @POST("/SRCLikes")
    Call<Respuesta> like(@Part("action") RequestBody action, @Part("like") RequestBody like);

    @Multipart
    @POST("/SRCFilter")
    Call<ArrayList<Recepta>> filtros(@Part("filtres") RequestBody filtro, @Part("usuari") RequestBody usuario);

    @Multipart
    @POST("/SRCRecepta")
    Call<Respuesta> crear_receta(@Part("action") RequestBody action, @Part("recepta") RequestBody recepta);

    @Multipart
    @POST("/SRCRecepta")
    Call<Recepta> select_receta(@Part("action") RequestBody action, @Part("recepta") RequestBody recepta, @Part("idUsuari") RequestBody idUsuari);

    @Multipart
    @POST("/SRCRecepta")
    Call<Respuesta> delete_receta(@Part("action") RequestBody action, @Part("recepta") RequestBody recepta);

    @Multipart
    @POST("/SRCFilter")
    Call<ArrayList<Recepta>> mis_recetas(@Part("filtres") RequestBody filtro, @Part("usuari") RequestBody usuario);

    @Multipart
    @POST("/SRCSeguidor")
    Call<Respuesta> seguir_usuario(@Part("action") RequestBody action, @Part("seguidor") RequestBody seguidor);

    @Multipart
    @POST("/SRCLikes")
    Call<Respuesta> like_receta(@Part("action") RequestBody action, @Part("like") RequestBody seguidor);

    @Multipart
    @POST("/SRCUserProfile")
    Call<Respuesta> crea_usuario(@Part("action") RequestBody action, @Part("usuario") RequestBody usuario);

    @Multipart
    @POST("/SRCUserProfile")
    Call<Usuari> login_usuario(@Part("action") RequestBody action, @Part("usuario") RequestBody usuario);

    @Multipart
    @POST("/SRCUserProfile")
    Call<Usuari> select_usuario(@Part("action") RequestBody action, @Part("usuario") RequestBody usuario);

    @Multipart
    @POST("/SRCUserProfile")
    Call<Respuesta> recuperacion_pass(@Part("action") RequestBody action, @Part("usuario") RequestBody usuario);

    @Multipart
    @POST("/SRCUserProfile")
    Call<Respuesta> editar_usuario(@Part("action") RequestBody action, @Part("usuario") RequestBody usuario_pass, @Part("usuarioModificado") RequestBody usuario);

    @Multipart
    @POST("/SRCSeguidor")
    Call<ArrayList<Usuari>> usuarios_seguidos(@Part("action") RequestBody action, @Part("seguidor") RequestBody usuario);
}