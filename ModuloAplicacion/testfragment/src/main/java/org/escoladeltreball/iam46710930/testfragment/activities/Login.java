package org.escoladeltreball.iam46710930.testfragment.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;
import org.escoladeltreball.iam46710930.testfragment.pojos.Action;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class Login extends Activity {

    @Bind(R.id.nombre_usuario_login)
    EditText nombreUsuarioLogin;
    @Bind(R.id.pass_usuario_login)
    EditText passUsuarioLogin;
    @Bind(R.id.title)
    TextView title;

    ConectionRetrofit conectionRetrofit;
    SharedPreferences prefe;
    Service service;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.carga);
        prefe = getSharedPreferences("data_login_receteame", Context.MODE_PRIVATE);
        conectionRetrofit = new ConectionRetrofit();
        service = conectionRetrofit.getService();
        String userGuardado = prefe.getString("token", "");
        if (!userGuardado.equals("")) {
            Usuari usuario = new Usuari();
            usuario.setToken(userGuardado);
            login_usuario(usuario, "loginTokenUsuario");
        } else {
            carga_layout();
        }
    }

    /**
     * Carga elementos layout
     */
    public void carga_layout() {
        setContentView(R.layout.login);
        ButterKnife.bind(this);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/playlist.otf");
        title.setTypeface(tf, Typeface.ITALIC);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    /**
     * Guarda en SharedPreferences la info para logueeo automático (token)
     *
     * @param usuario
     */
    public void guardaLogin(Usuari usuario) {
        SharedPreferences.Editor editor = prefe.edit();
        editor.putString("token", usuario.getToken());
        editor.putString("nombre_usuario", usuario.getNom_usuari());
        editor.putString("mail_usuario", usuario.getMail_usuari());
        editor.putString("id_usuario", usuario.getId_usuari().toString());
        editor.apply();
    }

    @OnClick({R.id.button_login, R.id.button_crear_cuenta, R.id.recordatorio_pass})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_login:
                login_usuario(check_campos(), "loginUsuario");
                break;
            case R.id.button_crear_cuenta:
                intent = new Intent(this, CreacionCuenta.class);
                startActivity(intent);
                break;
            case R.id.recordatorio_pass:
                intent = new Intent(this, RecuperacionPass.class);
                startActivity(intent);
                break;
        }
    }

    /**
     * Realiza consulta de inicio de sesión con el servidor
     *
     * @param usuario
     * @param action_login
     */
    public void login_usuario(Usuari usuario, final String action_login) {
        Gson gson = new Gson();
        if (usuario != null) {
            String jsonUsuario = gson.toJson(usuario);
            RequestBody requestUsuario = RequestBody.create(MediaType.parse("text/plain"), jsonUsuario);
            Action action = new Action(action_login);
            String jsonAction = gson.toJson(action);
            RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);
            Call<Usuari> call = service.login_usuario(requestAction, requestUsuario);
            call.enqueue(new Callback<Usuari>() {
                @Override
                public void onResponse(Response<Usuari> response) {
                    Usuari usuari = response.body();
                    if (usuari.getRespuesta().isRespuesta()) {
                        if (action_login.equals("loginUsuario")) {
                            guardaLogin(usuari);
                        }
                        lanzaApp();
                    } else {
                        if (!action_login.equals("loginUsuario")) {
                            carga_layout();
                        } else {
                            Toast.makeText(Login.this, "Usuario o contraseña incorrectos.", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Toast.makeText(Login.this, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                    carga_layout();
                    t.printStackTrace();
                    Log.e("delete", "onFailure", t);
                }
            });
        }
    }

    /**
     * Comprueba campos obligatórios
     *
     * @return
     */
    public Usuari check_campos() {
        Usuari usuario = new Usuari();
        if (nombreUsuarioLogin.getText().toString().trim().equals("")) {
            nombreUsuarioLogin.setError("Campo obligatorio");
            return null;
        } else if (passUsuarioLogin.getText().toString().trim().equals("")) {
            passUsuarioLogin.setError("Campo obligatorio");
            return null;
        } else {
            usuario.setNom_usuari(nombreUsuarioLogin.getText().toString().trim());
            usuario.setContrasenya_usuari(passUsuarioLogin.getText().toString().trim());
        }
        return usuario;
    }

    /**
     * Inicia aplicación
     */
    public void lanzaApp() {
        intent = new Intent(Login.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
