package org.escoladeltreball.iam46710930.testfragment.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.pojos.Action;
import org.escoladeltreball.iam46710930.testfragment.pojos.Respuesta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;
import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class RecuperacionPass extends Activity {

    ConectionRetrofit conectionRetrofit;
    Service service;

    @Bind(R.id.mail_recuperacion)
    EditText mailRecuperacion;
    @Bind(R.id.button_recuperacion_pass)
    Button buttonRecuperacionPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recuperacion_pass);
        ButterKnife.bind(this);
    }

    /**
     * Envia solicitud de recuperacion de contraseña por mail.
     *
     * @param usuario
     */
    public void recupera_pass(Usuari usuario) {
        Gson gson = new Gson();
        if (usuario != null) {
            usuario.setMail_usuari(mailRecuperacion.getText().toString());
            String jsonUsuario = gson.toJson(usuario);
            RequestBody requestUsuario = RequestBody.create(MediaType.parse("text/plain"), jsonUsuario);
            Action action = new Action("loginUsuario");
            String jsonAction = gson.toJson(action);
            RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);
            Call<Respuesta> call = service.recuperacion_pass(requestAction, requestUsuario);
            call.enqueue(new Callback<Respuesta>() {
                @Override
                public void onResponse(Response<Respuesta> response) {
                    Respuesta respuesta = response.body();
                    if (respuesta.isRespuesta()) {
                        Toast.makeText(RecuperacionPass.this, "El mail ha sido enviado", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(RecuperacionPass.this, "Mail no registrado", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                    Log.e("delete", "onFailure", t);
                }
            });
        }
    }

    /**
     * Comprueba campos obligatorios
     *
     * @return
     */
    public Usuari check_campos() {
        String mail = mailRecuperacion.getText().toString().trim();
        if (mail.equals("")) {
            mailRecuperacion.setError("introduzca el mail de su cuenta");
            return null;
        } else if (!mail.matches(".+@.+\\..+")) {
            mailRecuperacion.setError("mail incorrecto");
            return null;
        }
        Usuari usuario = new Usuari();
        usuario.setMail_usuari(mail);
        return usuario;
    }

    @OnClick(R.id.button_recuperacion_pass)
    public void onClick() {
        recupera_pass(check_campos());
    }
}
