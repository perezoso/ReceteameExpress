package org.escoladeltreball.iam46710930.testfragment.fragments;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.activities.MainActivity;
import org.escoladeltreball.iam46710930.testfragment.pojos.Action;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;
import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;

import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class FragmentPerfil extends Fragment {
    ConectionRetrofit conectionRetrofit;
    Service service;
    View rootView;
    MainActivity mainActivity;
    static SharedPreferences prefe;
    TextView nombreUsuarioPerfil;
    TextView mailUsuarioPerfil;
    TextView cantidadRecetas;
    TextView cantidadSeguidores;
    TextView cantidadLikes;

    public static FragmentPerfil newInstance(SharedPreferences sharedPreferences) {
        FragmentPerfil fragment_perfil = new FragmentPerfil();
        prefe = sharedPreferences;
        return fragment_perfil;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.perfil, container, false);
        conectionRetrofit = new ConectionRetrofit();
        service = conectionRetrofit.getService();
        mainActivity = (MainActivity) getActivity();

        nombreUsuarioPerfil = (TextView) rootView.findViewById(R.id.nombre_usuario_perfil);
        mailUsuarioPerfil = (TextView) rootView.findViewById(R.id.mail_usuario_perfil);
        cantidadRecetas = (TextView) rootView.findViewById(R.id.cantidad_recetas);
        cantidadSeguidores = (TextView) rootView.findViewById(R.id.cantidad_seguidores);
        cantidadLikes = (TextView) rootView.findViewById(R.id.cantidad_likes);

        ButterKnife.bind(this, rootView);
        selectUsuario();
        return rootView;
    }

    /**
     * Realiza petición de información de perfil al servidor
     */
    public void selectUsuario() {
        Gson gson = new Gson();
        Usuari usuario = new Usuari();
        usuario.setId_usuari(Integer.parseInt(prefe.getString("id_usuario", "")));
        String jsonUsuario = gson.toJson(usuario);
        RequestBody requestUsuario = RequestBody.create(MediaType.parse("text/plain"), jsonUsuario);

        Action action = new Action("selectUsuario");
        String jsonAction = gson.toJson(action);
        RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

        Call<Usuari> call = service.select_usuario(requestAction, requestUsuario);
        call.enqueue(new Callback<Usuari>() {
            @Override
            public void onResponse(Response<Usuari> response) {
                Usuari usuario = response.body();
                if (rootView != null) {
                    carga_contenido(usuario);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                Log.e("MainActivity", "onFailure", t);
            }
        });
    }

    /**
     * Carga información pèrfil respuesta servidor
     *
     * @param usuario
     */
    public void carga_contenido(Usuari usuario) {
        nombreUsuarioPerfil.setText(prefe.getString("nombre_usuario", ""));
        mailUsuarioPerfil.setText(prefe.getString("mail_usuario", ""));
        cantidadRecetas.setText(usuario.getCantidadReceptas().toString());
        cantidadSeguidores.setText(usuario.getCantidadSeguidores().toString());
        cantidadLikes.setText(usuario.getCantidadLikes().toString());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.edit_perfil)
    public void onClick() {
        mainActivity.cambia_fragment(FragmentEditarPerfil.newInstance(prefe));
    }

    @Override
    public void onDetach() {
        mainActivity = null;
        rootView = null;
        super.onDetach();
    }
}
