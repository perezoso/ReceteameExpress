package org.escoladeltreball.iam46710930.testfragment.pojos;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class InfoPrincipalReceta {

    private EditText titolRecepta;
    private Spinner categoriaMenu;
    private EditText numComensales;
    private TextView numVisibIngredientes;
    private TextView numVisibPasos;
    private ImageView imagenReceta;

    public InfoPrincipalReceta(EditText titolRecepta, Spinner categoriaMenu, EditText numComensales, TextView numVisibIngredientes, TextView numVisibPasos, ImageView imagenReceta) {
        this.titolRecepta = titolRecepta;
        this.categoriaMenu = categoriaMenu;
        this.numComensales = numComensales;
        this.numVisibIngredientes = numVisibIngredientes;
        this.numVisibPasos = numVisibPasos;
        this.imagenReceta = imagenReceta;
    }

    public EditText getTitolRecepta() {
        return titolRecepta;
    }

    public void setTitolRecepta(EditText titolRecepta) {
        this.titolRecepta = titolRecepta;
    }

    public Spinner getCategoriaMenu() {
        return categoriaMenu;
    }

    public void setCategoriaMenu(Spinner categoriaMenu) {
        this.categoriaMenu = categoriaMenu;
    }

    public EditText getNumComensales() {
        return numComensales;
    }

    public void setNumComensales(EditText numComensales) {
        this.numComensales = numComensales;
    }

    public TextView getNumVisibIngredientes() {
        return numVisibIngredientes;
    }

    public void setNumVisibIngredientes(TextView numVisibIngredientes) {
        this.numVisibIngredientes = numVisibIngredientes;
    }

    public TextView getNumVisibPasos() {
        return numVisibPasos;
    }

    public void setNumVisibPasos(TextView numVisibPasos) {
        this.numVisibPasos = numVisibPasos;
    }

    public ImageView getImagenReceta() {
        return imagenReceta;
    }

    public void setImagenReceta(ImageView imagenReceta) {
        this.imagenReceta = imagenReceta;
    }
}
