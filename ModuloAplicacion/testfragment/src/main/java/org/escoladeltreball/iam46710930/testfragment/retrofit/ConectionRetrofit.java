package org.escoladeltreball.iam46710930.testfragment.retrofit;


import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class ConectionRetrofit {

    private Retrofit retrofit;
    private Service service;

    public ConectionRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://receteame.herokuapp.com")

                .addConverterFactory(GsonConverterFactory.create())

                .build();
        service = retrofit.create(Service.class);
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
