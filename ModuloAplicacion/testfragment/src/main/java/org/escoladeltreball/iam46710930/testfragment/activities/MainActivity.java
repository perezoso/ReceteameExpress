package org.escoladeltreball.iam46710930.testfragment.activities;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.fragments.FragmentListadoRecetas;
import org.escoladeltreball.iam46710930.testfragment.fragments.FragmentMisRecetas;
import org.escoladeltreball.iam46710930.testfragment.fragments.FragmentPerfil;
import org.escoladeltreball.iam46710930.testfragment.fragments.FragmentUsuariosSeguidos;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ActionBar.OnNavigationListener, AdapterView.OnItemSelectedListener {
    View spinnerContainer;
    Fragment fragment = null;
    Fragment fragment_anterior = null;
    Resources resources;
    Toolbar toolbar;
    Spinner spinner;
    boolean spinner_filtros_carga = false;
    SharedPreferences prefe;
    boolean doubleBackToExitPressedOnce = false;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        prefe = getSharedPreferences("data_login_receteame", Context.MODE_PRIVATE);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        resources = getResources();
        inicio_fragment_inicial();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        int itemID = menuItem.getItemId();
        fragment_anterior = fragment;
        switch (itemID) {
            case R.id.nav_listados_recetas:
                fragment = FragmentListadoRecetas.newInstance(resources.getStringArray(R.array.menu_filtros)[0], prefe);
                if (!spinner_filtros_carga) {
                    spinnerContainer.setVisibility(View.VISIBLE);
                    spinner.setSelection(0);
                    spinner_filtros_carga = true;
                }
                spinner.setSelection(0);
                break;
            case R.id.nav_perfil:
                controlSpinnerFiltros();
                fragment = FragmentPerfil.newInstance(prefe);
                break;
            case R.id.nav_seguidos:
                controlSpinnerFiltros();
                fragment = FragmentUsuariosSeguidos.newInstance(prefe, false);
                break;
            case R.id.nav_mis_recetas:
                controlSpinnerFiltros();
                fragment = FragmentMisRecetas.newInstance(prefe, 0);
                break;
            case R.id.nav_logout:
                borraLogin();
                Intent intent = new Intent(this, Login.class);
                startActivity(intent);
                finish();
                break;
        }
        cambia_fragment_menu();
        setTitle(menuItem.getTitle());
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Comprueba visibilidad spinner filtros listado recetas y lo esconde
     */
    public void controlSpinnerFiltros() {
        if (spinner_filtros_carga) {
            spinnerContainer.setVisibility(View.GONE);
            spinner_filtros_carga = false;
        }
    }

    /**
     * Método inicio fragment inicial.
     */
    public void inicio_fragment_inicial() {
        fragment = FragmentListadoRecetas.newInstance(resources.getStringArray(R.array.menu_filtros)[0], prefe);
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, fragment, "inicio").addToBackStack("menu").commit();
        setTitle("Recetas");
        carga_spinner();
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        fragment = FragmentListadoRecetas.newInstance(resources.getStringArray(R.array.menu_filtros)[position], prefe);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    /**
     * Carga spinner para fragment listado recetas
     */
    public void carga_spinner() {
        spinnerContainer = LayoutInflater.from(this).inflate(R.layout.toolbar_spinner,
                toolbar, false);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        toolbar.addView(spinnerContainer, lp);
        ArrayAdapter<String> aAdpt = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, resources.getStringArray(R.array.menu_filtros));
        spinner = (Spinner) spinnerContainer.findViewById(R.id.toolbar_spinner);
        spinner.setAdapter(aAdpt);
        spinner.setOnItemSelectedListener(this);
        spinner_filtros_carga = true;
    }

    /**
     * Método de reemplace de fragments menu principal.
     */
    public void cambia_fragment_menu() {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            clean_back_stack();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment, "inicio").addToBackStack("menu").commit();
        }
    }

    /**
     * Método de reemplace de fragments para su profundidad.
     *
     * @param fragment
     */
    public void cambia_fragment(Fragment fragment) {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            getSupportFragmentManager().beginTransaction().replace(R.id.flContent, fragment).addToBackStack("extra").commit();
        } else {
            getSupportFragmentManager().popBackStackImmediate();
            getSupportFragmentManager().beginTransaction().replace(R.id.flContent, fragment).addToBackStack("extra").commit();
        }
        controlSpinnerFiltros();
    }

    /**
     * Método limpia backStack de fragments para el control de profundidad "onBackPressed"
     */
    public void clean_back_stack() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            getSupportFragmentManager().popBackStackImmediate();
        }

    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            if (count == 1) {
                exit();
            } else {
                getSupportFragmentManager().popBackStackImmediate();

                count = getSupportFragmentManager().getBackStackEntryCount();
                if (count == 0) {
                    super.onBackPressed();
                } else {
                    if (fragment instanceof FragmentListadoRecetas) {
                        spinnerContainer.setVisibility(View.VISIBLE);
                        spinner.setSelection(0);
                        spinner_filtros_carga = true;

                    } else if (fragment instanceof FragmentUsuariosSeguidos) {
                        fragment = FragmentUsuariosSeguidos.newInstance(prefe, false);
                        cambia_fragment_menu();
                    }
                }
            }
        }
    }

    /**
     * Método que realiza el control de doble pulsación para salir
     */
    public void exit() {
        if (doubleBackToExitPressedOnce) {
            getSupportFragmentManager().popBackStackImmediate();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Pulse de nuevo para salir", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    /**
     * Borra token de sharedPreferences para evitar el login automático
     */
    public void borraLogin() {
        SharedPreferences.Editor editor = prefe.edit();
        editor.remove("token");
        editor.apply();
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }
}

