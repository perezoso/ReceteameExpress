package org.escoladeltreball.iam46710930.testfragment.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.activities.MainActivity;
import org.escoladeltreball.iam46710930.testfragment.pojos.Action;
import org.escoladeltreball.iam46710930.testfragment.pojos.InfoPrincipalReceta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Ingredient;
import org.escoladeltreball.iam46710930.testfragment.pojos.LineaIngredienteInsercion;
import org.escoladeltreball.iam46710930.testfragment.pojos.LineaPasoInsercion;
import org.escoladeltreball.iam46710930.testfragment.pojos.Pas;
import org.escoladeltreball.iam46710930.testfragment.pojos.Recepta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Respuesta;
import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class FragmentInsercionReceta extends Fragment {
    @Bind(R.id.titol_recepta)
    EditText titolRecepta;
    @Bind(R.id.categoria_menu)
    Spinner categoriaMenu;
    @Bind(R.id.num_comensales)
    EditText numComensales;
    @Bind(R.id.num_visib_ingredientes)
    TextView numVisibIngredientes;
    @Bind(R.id.nombre_ingrediente_1)
    EditText nombreIngrediente1;
    @Bind(R.id.cantidad_ingrediente_1)
    EditText cantidadIngrediente1;
    @Bind(R.id.tipo_cantidad_ingrediente_1)
    Spinner tipoCantidadIngrediente1;
    @Bind(R.id.nombre_ingrediente_2)
    EditText nombreIngrediente2;
    @Bind(R.id.cantidad_ingrediente_2)
    EditText cantidadIngrediente2;
    @Bind(R.id.tipo_cantidad_ingrediente_2)
    Spinner tipoCantidadIngrediente2;
    @Bind(R.id.nombre_ingrediente_3)
    EditText nombreIngrediente3;
    @Bind(R.id.cantidad_ingrediente_3)
    EditText cantidadIngrediente3;
    @Bind(R.id.tipo_cantidad_ingrediente_3)
    Spinner tipoCantidadIngrediente3;
    @Bind(R.id.nombre_ingrediente_4)
    EditText nombreIngrediente4;
    @Bind(R.id.cantidad_ingrediente_4)
    EditText cantidadIngrediente4;
    @Bind(R.id.tipo_cantidad_ingrediente_4)
    Spinner tipoCantidadIngrediente4;
    @Bind(R.id.nombre_ingrediente_5)
    EditText nombreIngrediente5;
    @Bind(R.id.cantidad_ingrediente_5)
    EditText cantidadIngrediente5;
    @Bind(R.id.tipo_cantidad_ingrediente_5)
    Spinner tipoCantidadIngrediente5;
    @Bind(R.id.nombre_ingrediente_6)
    EditText nombreIngrediente6;
    @Bind(R.id.cantidad_ingrediente_6)
    EditText cantidadIngrediente6;
    @Bind(R.id.tipo_cantidad_ingrediente_6)
    Spinner tipoCantidadIngrediente6;
    @Bind(R.id.imagen_receta)
    ImageView imagenReceta;
    @Bind(R.id.num_visib_pasos)
    TextView numVisibPasos;
    @Bind(R.id.descripcion_paso_1)
    EditText descripcionPaso1;
    @Bind(R.id.imagen_paso_1)
    ImageView imagenPaso1;
    @Bind(R.id.linea_paso_1_2)
    View lineaPaso12;
    @Bind(R.id.descripcion_paso_2)
    EditText descripcionPaso2;
    @Bind(R.id.imagen_paso_2)
    ImageView imagenPaso2;
    @Bind(R.id.linea_paso_2_3)
    View lineaPaso23;
    @Bind(R.id.descripcion_paso_3)
    EditText descripcionPaso3;
    @Bind(R.id.imagen_paso_3)
    ImageView imagenPaso3;
    @Bind(R.id.button_crear)
    Button buttonCrear;
    @Bind(R.id.titol_edicion)
    TextView titolEdicion;

    ArrayList<LineaIngredienteInsercion> lista_lineas_ingrdientes;
    ArrayList<LineaPasoInsercion> lista_lineas_pasos;
    ArrayList<Boolean> check_imagenes_insertadas;

    InfoPrincipalReceta info_principal_receta;

    static SharedPreferences prefe;
    static Recepta receta_propia;
    static boolean check_edit_receta;

    EditReceta edit_receta;
    Respuesta respuesta;

    ConectionRetrofit conectionRetrofit;
    View rootView;

    MainActivity mainActivity;

    ArrayList<String> array_categoria_menu;
    ArrayList<String> array_tipo_cantidad;

    public static FragmentInsercionReceta newInstance(boolean edit_receta, Recepta receta, SharedPreferences sharedPreferences) {
        if (edit_receta) {
            receta_propia = receta;
        }
        prefe = sharedPreferences;
        check_edit_receta = edit_receta;
        FragmentInsercionReceta fragment_insercion_receta = new FragmentInsercionReceta();
        return fragment_insercion_receta;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.insercion_receta, container, false);

        conectionRetrofit = new ConectionRetrofit();
        ButterKnife.bind(this, rootView);

        carga_elementos_grupos();

        mainActivity = (MainActivity) getActivity();

        if (check_edit_receta && rootView != null) {
            array_categoria_menu = new ArrayList<>(Arrays.asList(mainActivity.getResources().getStringArray(R.array.categoria_menu)));
            array_tipo_cantidad = new ArrayList<>(Arrays.asList(mainActivity.getResources().getStringArray(R.array.tipo_cantidad)));
            carga_receta_editar();
            buttonCrear.setText("guardar");
            titolEdicion.setText("Edición");
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * Carga elementos layaout en listas para su manipulación
     */
    public void carga_elementos_grupos() {
        info_principal_receta = new InfoPrincipalReceta(titolRecepta, categoriaMenu, numComensales, numVisibIngredientes, numVisibPasos, imagenReceta);

        lista_lineas_ingrdientes = new ArrayList<>();

        lista_lineas_ingrdientes.add(new LineaIngredienteInsercion(nombreIngrediente1, cantidadIngrediente1, tipoCantidadIngrediente1));
        lista_lineas_ingrdientes.add(new LineaIngredienteInsercion(nombreIngrediente2, cantidadIngrediente2, tipoCantidadIngrediente2));
        lista_lineas_ingrdientes.add(new LineaIngredienteInsercion(nombreIngrediente3, cantidadIngrediente3, tipoCantidadIngrediente3));
        lista_lineas_ingrdientes.add(new LineaIngredienteInsercion(nombreIngrediente4, cantidadIngrediente4, tipoCantidadIngrediente4));
        lista_lineas_ingrdientes.add(new LineaIngredienteInsercion(nombreIngrediente5, cantidadIngrediente5, tipoCantidadIngrediente5));
        lista_lineas_ingrdientes.add(new LineaIngredienteInsercion(nombreIngrediente6, cantidadIngrediente6, tipoCantidadIngrediente6));

        lista_lineas_pasos = new ArrayList<>();
        lista_lineas_pasos.add(new LineaPasoInsercion(descripcionPaso1, imagenPaso1));
        lista_lineas_pasos.add(new LineaPasoInsercion(descripcionPaso2, imagenPaso2, lineaPaso12));
        lista_lineas_pasos.add(new LineaPasoInsercion(descripcionPaso3, imagenPaso3, lineaPaso23));

        check_imagenes_insertadas = new ArrayList<Boolean>() {{
            add(false);
            add(false);
            add(false);
            add(false);
        }};
    }

    /**
     * Carga información ya existente de la receta en caso de estar editando la receta
     */
    public void carga_receta_editar() {
        edit_receta = new EditReceta(receta_propia, info_principal_receta, lista_lineas_ingrdientes, lista_lineas_pasos, check_imagenes_insertadas, array_categoria_menu, array_tipo_cantidad);
        check_imagenes_insertadas = edit_receta.check_imagenes();
    }

    @OnClick({R.id.imagen_receta, R.id.button_plus_ingrediente, R.id.button_less_ingrediente, R.id.imagen_paso_1, R.id.imagen_paso_2, R.id.imagen_paso_3, R.id.button_plus_paso, R.id.button_less_paso, R.id.button_crear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_plus_ingrediente:
                for (int i = 1; i < lista_lineas_ingrdientes.size(); i++) {
                    LineaIngredienteInsercion linea_ingrediente = lista_lineas_ingrdientes.get(i);
                    if (linea_ingrediente.getNombre_ingrediente().getVisibility() == View.GONE) {
                        linea_ingrediente.getNombre_ingrediente().setVisibility(View.VISIBLE);
                        linea_ingrediente.getCantidad_ingrediente().setVisibility(View.VISIBLE);
                        linea_ingrediente.getTipo_cantidad().setVisibility(View.VISIBLE);
                        numVisibIngredientes.setText(i + 1 + "/6");
                        break;
                    }
                }
                break;
            case R.id.button_less_ingrediente:
                for (int i = lista_lineas_ingrdientes.size() - 1; i >= 1; i--) {
                    LineaIngredienteInsercion linea_ingrediente = lista_lineas_ingrdientes.get(i);
                    if (linea_ingrediente.getNombre_ingrediente().getVisibility() == View.VISIBLE) {
                        linea_ingrediente.getNombre_ingrediente().setVisibility(View.GONE);
                        linea_ingrediente.getCantidad_ingrediente().setVisibility(View.GONE);
                        linea_ingrediente.getTipo_cantidad().setVisibility(View.GONE);
                        numVisibIngredientes.setText(i + "/6");
                        break;
                    }
                }
                break;
            case R.id.imagen_receta:
                verifica_pass(0);
                break;
            case R.id.imagen_paso_1:
                verifica_pass(1);
                break;
            case R.id.imagen_paso_2:
                verifica_pass(2);
                break;
            case R.id.imagen_paso_3:
                verifica_pass(3);
                break;
            case R.id.button_plus_paso:
                for (int i = 1; i < lista_lineas_pasos.size(); i++) {
                    LineaPasoInsercion linea_paso = lista_lineas_pasos.get(i);
                    if (linea_paso.getDescripcion_paso().getVisibility() == View.GONE) {
                        linea_paso.getDescripcion_paso().setVisibility(View.VISIBLE);
                        linea_paso.getImagen_paso().setVisibility(View.VISIBLE);
                        linea_paso.getLinea_separacion().setVisibility(View.VISIBLE);
                        numVisibPasos.setText(i + 1 + "/3");
                        break;
                    }
                }
                break;
            case R.id.button_less_paso:
                for (int i = lista_lineas_pasos.size() - 1; i >= 1; i--) {
                    LineaPasoInsercion linea_paso = lista_lineas_pasos.get(i);
                    if (linea_paso.getDescripcion_paso().getVisibility() == View.VISIBLE) {
                        linea_paso.getDescripcion_paso().setVisibility(View.GONE);
                        linea_paso.getImagen_paso().setVisibility(View.GONE);
                        linea_paso.getLinea_separacion().setVisibility(View.GONE);
                        numVisibPasos.setText(i + "/3");
                        break;
                    }
                }
                break;
            case R.id.button_crear:
                crear_receta(conectionRetrofit.getService());
                break;
        }
    }

    /**
     * Envia la información al servidor para su inserción o actualización
     *
     * @param service
     */
    public void crear_receta(Service service) {
        Gson gson = new Gson();
        Action action;
        if (check_edit_receta) {
            action = new Action("updateRecepta");
        } else {
            action = new Action("insert");
        }
        Recepta recepta = new Recepta();
        recepta = carga_receta(recepta);
        if (recepta != null) {
            Toast.makeText(mainActivity, "Iniciando guardado de datos.", Toast.LENGTH_LONG).show();
            String json_receta = gson.toJson(recepta);
            RequestBody request_receta = RequestBody.create(MediaType.parse("text/plain"), json_receta);
            String json_action = gson.toJson(action);
            RequestBody request_action = RequestBody.create(MediaType.parse("text/plain"), json_action);
            Call<Respuesta> call = service.crear_receta(request_action, request_receta);
            call.enqueue(new Callback<Respuesta>() {
                @Override
                public void onResponse(Response<Respuesta> response) {
                    respuesta = response.body();
                    Toast.makeText(getContext(), "Datos guardados.", Toast.LENGTH_LONG).show();
                    if (respuesta.isRespuesta()) {
                        mainActivity.setFragment(FragmentMisRecetas.newInstance(prefe, 0));
                        mainActivity.cambia_fragment_menu();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                    t.printStackTrace();
                    Log.e("MainActivity", "onFailure", t);
                }
            });
        }
    }

    /**
     * Comprueba campos para su inserción
     *
     * @param receta
     * @return
     */
    public Recepta carga_receta(Recepta receta) {
        String campo_verificar_string = titolRecepta.getText().toString().trim();
        if (campo_verificar_string.equals("")) {
            titolRecepta.setError("Campo obligatorio.");
            titolRecepta.requestFocus();
            return null;
        } else {
            receta.setTitol_recepta(campo_verificar_string);
        }
        campo_verificar_string = numComensales.getText().toString().trim();
        if (campo_verificar_string.equals("")) {
            numComensales.setError("Campo obligatorio.");
            numComensales.requestFocus();
            return null;
        } else {
            receta.setNumero_comensals(Integer.parseInt(campo_verificar_string));
        }
        ArrayList<Ingredient> lista_ingredientes = new ArrayList<>();
        Ingredient ingrediente;
        int countIngrediente = 1;
        for (LineaIngredienteInsercion linea_ingrediente : lista_lineas_ingrdientes) {
            if (linea_ingrediente.getNombre_ingrediente().getVisibility() == View.VISIBLE) {
                if (linea_ingrediente.getNombre_ingrediente().getText().toString().trim().equals("") ||
                        linea_ingrediente.getCantidad_ingrediente().getText().toString().trim().equals("")) {
                    if (countIngrediente == 1) {
                        linea_ingrediente.getNombre_ingrediente().setError("Obligatorio mínimo 1 ingerdiente.");
                        linea_ingrediente.getNombre_ingrediente().requestFocus();
                    } else {
                        linea_ingrediente.getNombre_ingrediente().setError("Rellene o elimine el ingrediente.");
                        linea_ingrediente.getNombre_ingrediente().requestFocus();
                    }
                    return null;
                }
                ingrediente = new Ingredient(linea_ingrediente.getNombre_ingrediente().getText().toString(),
                        Integer.parseInt(linea_ingrediente.getCantidad_ingrediente().getText().toString()),
                        linea_ingrediente.getTipo_cantidad().getSelectedItem().toString());
                if (check_edit_receta) {
                    ingrediente.setId_recepta(receta_propia.getId_recepta());
                }
                lista_ingredientes.add(ingrediente);
                countIngrediente++;
            }
        }
        ArrayList<Pas> lista_pasos = new ArrayList<>();
        int count_pasos = 1;
        byte[] imagen;
        Pas paso;
        for (LineaPasoInsercion linea_paso : lista_lineas_pasos) {
            if (linea_paso.getDescripcion_paso().getVisibility() == View.VISIBLE) {

                linea_paso.getImagen_paso().setDrawingCacheEnabled(true);
                imagen = convert_bitmap_bytes(linea_paso.getImagen_paso().getDrawingCache());
                if (linea_paso.getDescripcion_paso().getText().toString().trim().equals("")) {

                    if (count_pasos == 1) {
                        linea_paso.getDescripcion_paso().setError("Obligatorio mínimo 1 paso.");
                        linea_paso.getDescripcion_paso().requestFocus();
                    } else {
                        linea_paso.getDescripcion_paso().setError("Rellene o elimine el paso.");
                        linea_paso.getDescripcion_paso().requestFocus();
                    }
                    return null;
                }
                paso = new Pas(linea_paso.getDescripcion_paso().getText().toString(), imagen);
                if (check_edit_receta) {
                    paso.setId_recepta(receta_propia.getId_recepta());
                }
                lista_pasos.add(paso);
                count_pasos++;
            }
        }
        receta.setId_usuari(Integer.parseInt(prefe.getString("id_usuario", "")));
        receta.setCategoria_menu(categoriaMenu.getSelectedItem().toString());
        receta.setListIngredients(lista_ingredientes);
        receta.setListPassos(lista_pasos);
        imagenReceta.setDrawingCacheEnabled(true);
        receta.setImatge_recepta(convert_bitmap_bytes(imagenReceta.getDrawingCache()));
        if (check_edit_receta) {
            receta.setId_recepta(receta_propia.getId_recepta());
        }
        return receta;
    }

    /**
     * Método para pasar un bitmap a byte[]
     *
     * @param bitmap
     * @return
     */
    public byte[] convert_bitmap_bytes(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] bytes = baos.toByteArray();
        return bytes;
    }

    /**
     * Abre un alertdialog para que el usuario escoje si realizar la selección de la imagen de la galeria o realizarla con la camara
     *
     * @param id_image
     */
    public void verifica_pass(final int id_image) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Imagen");
        String positiveText = "Galeria";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent image_intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                        startActivityForResult(image_intent, id_image);
                    }
                });
        String negativeText = "Camara";
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent image_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(image_intent, id_image + 10);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();
            InputStream is;
            boolean take_picture = false;
            if (requestCode >= 10) {
                requestCode = requestCode - 10;
                take_picture = true;
            }
            switch (requestCode) {
                case 0:
                    if (take_picture) {
                        if (data.hasExtra("data")) {
                            Bitmap bitmap = (Bitmap) data.getParcelableExtra("data");
                            bitmap = scaleToFitWidth(bitmap, 300);
                            imagenReceta.setImageBitmap(bitmap);
                        }
                    } else {
                        try {
                            is = getContext().getContentResolver().openInputStream(selectedImage);
                            BufferedInputStream bis = new BufferedInputStream(is);
                            Bitmap bitmap = BitmapFactory.decodeStream(bis);
                            bitmap = scaleToFitWidth(bitmap, 300);
                            imagenReceta.setImageBitmap(bitmap);
                            check_imagenes_insertadas.set(requestCode, true);
                        } catch (FileNotFoundException e) {
                            Log.e("carga imagen", "click", e);
                        }
                    }
                    break;
                case 1:
                    if (take_picture) {
                        if (data.hasExtra("data")) {
                            Bitmap bitmap = (Bitmap) data.getParcelableExtra("data");
                            bitmap = scaleToFitWidth(bitmap, 300);
                            imagenPaso1.setImageBitmap(bitmap);
                        }
                    } else {
                        try {
                            is = getContext().getContentResolver().openInputStream(selectedImage);
                            BufferedInputStream bis = new BufferedInputStream(is);
                            Bitmap bitmap = BitmapFactory.decodeStream(bis);
                            bitmap = scaleToFitWidth(bitmap, 300);
                            imagenPaso1.setImageBitmap(bitmap);
                            check_imagenes_insertadas.set(requestCode, true);
                        } catch (FileNotFoundException e) {
                            Log.e("carga imagen", "click", e);
                        }
                    }
                    break;
                case 2:
                    if (take_picture) {
                        if (data.hasExtra("data")) {
                            Bitmap bitmap = (Bitmap) data.getParcelableExtra("data");
                            bitmap = scaleToFitWidth(bitmap, 300);
                            imagenPaso2.setImageBitmap(bitmap);
                        }
                    } else {
                        try {
                            is = getContext().getContentResolver().openInputStream(selectedImage);
                            BufferedInputStream bis = new BufferedInputStream(is);
                            Bitmap bitmap = BitmapFactory.decodeStream(bis);
                            bitmap = scaleToFitWidth(bitmap, 300);
                            imagenPaso2.setImageBitmap(bitmap);
                            check_imagenes_insertadas.set(requestCode, true);
                        } catch (FileNotFoundException e) {
                            Log.e("carga imagen", "click", e);
                        }
                    }
                    break;
                case 3:
                    if (take_picture) {
                        if (data.hasExtra("data")) {
                            Bitmap bitmap = (Bitmap) data.getParcelableExtra("data");
                            bitmap = scaleToFitWidth(bitmap, 300);
                            imagenPaso3.setImageBitmap(bitmap);
                        }
                    } else {
                        try {
                            is = getContext().getContentResolver().openInputStream(selectedImage);
                            BufferedInputStream bis = new BufferedInputStream(is);
                            Bitmap bitmap = BitmapFactory.decodeStream(bis);
                            bitmap = scaleToFitWidth(bitmap, 300);
                            imagenPaso3.setImageBitmap(bitmap);
                            check_imagenes_insertadas.set(requestCode, true);
                        } catch (FileNotFoundException e) {
                            Log.e("carga imagen", "click", e);
                        }
                    }
                    break;
            }
        }
    }

    /**
     * Método que realiza un resize de un bitmap escalado por width
     *
     * @param b
     * @param width
     * @return
     */
    public static Bitmap scaleToFitWidth(Bitmap b, int width) {
        float factor = width / (float) b.getWidth();
        return Bitmap.createScaledBitmap(b, width, (int) (b.getHeight() * factor), true);
    }

    @Override
    public void onDetach() {
        mainActivity = null;
        rootView = null;
        super.onDetach();
    }
}
