package org.escoladeltreball.iam46710930.testfragment.fragments;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.activities.MainActivity;
import org.escoladeltreball.iam46710930.testfragment.pojos.Action;
import org.escoladeltreball.iam46710930.testfragment.pojos.Ingredient;
import org.escoladeltreball.iam46710930.testfragment.pojos.Like;
import org.escoladeltreball.iam46710930.testfragment.pojos.LineaIngredienteVisualizacion;
import org.escoladeltreball.iam46710930.testfragment.pojos.LineaPasoVisualizacion;
import org.escoladeltreball.iam46710930.testfragment.pojos.Pas;
import org.escoladeltreball.iam46710930.testfragment.pojos.Recepta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Respuesta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Seguidor;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;
import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class FragmentVisualizacionReceta extends Fragment {

    @Bind(R.id.titol_recepta)
    TextView titolRecepta;
    @Bind(R.id.imagen_receta)
    ImageView imagenReceta;
    @Bind(R.id.categoria_menu)
    TextView categoriaMenu;
    @Bind(R.id.num_comensales)
    TextView numComensales;
    @Bind(R.id.nombre_ingrediente_1)
    TextView nombreIngrediente1;
    @Bind(R.id.cantidad_ingrediente_1)
    TextView cantidadIngrediente1;
    @Bind(R.id.tipo_cantidad_ingrediente_1)
    TextView tipoCantidadIngrediente1;
    @Bind(R.id.nombre_ingrediente_2)
    TextView nombreIngrediente2;
    @Bind(R.id.cantidad_ingrediente_2)
    TextView cantidadIngrediente2;
    @Bind(R.id.tipo_cantidad_ingrediente_2)
    TextView tipoCantidadIngrediente2;
    @Bind(R.id.nombre_ingrediente_3)
    TextView nombreIngrediente3;
    @Bind(R.id.cantidad_ingrediente_3)
    TextView cantidadIngrediente3;
    @Bind(R.id.tipo_cantidad_ingrediente_3)
    TextView tipoCantidadIngrediente3;
    @Bind(R.id.nombre_ingrediente_4)
    TextView nombreIngrediente4;
    @Bind(R.id.cantidad_ingrediente_4)
    TextView cantidadIngrediente4;
    @Bind(R.id.tipo_cantidad_ingrediente_4)
    TextView tipoCantidadIngrediente4;
    @Bind(R.id.nombre_ingrediente_5)
    TextView nombreIngrediente5;
    @Bind(R.id.cantidad_ingrediente_5)
    TextView cantidadIngrediente5;
    @Bind(R.id.tipo_cantidad_ingrediente_5)
    TextView tipoCantidadIngrediente5;
    @Bind(R.id.nombre_ingrediente_6)
    TextView nombreIngrediente6;
    @Bind(R.id.cantidad_ingrediente_6)
    TextView cantidadIngrediente6;
    @Bind(R.id.tipo_cantidad_ingrediente_6)
    TextView tipoCantidadIngrediente6;
    @Bind(R.id.descripcion_paso_1)
    TextView descripcionPaso1;
    @Bind(R.id.imagen_paso_1)
    ImageView imagenPaso1;
    @Bind(R.id.linea_paso_1_2)
    View lineaPaso12;
    @Bind(R.id.descripcion_paso_2)
    TextView descripcionPaso2;
    @Bind(R.id.imagen_paso_2)
    ImageView imagenPaso2;
    @Bind(R.id.linea_paso_2_3)
    View lineaPaso23;
    @Bind(R.id.descripcion_paso_3)
    TextView descripcionPaso3;
    @Bind(R.id.imagen_paso_3)
    ImageView imagenPaso3;
    @Bind(R.id.star)
    CheckBox star;
    @Bind(R.id.nombre_creador)
    TextView nombreCreador;
    @Bind(R.id.like)
    CheckBox like;
    @Bind(R.id.contador_likes)
    TextView contadorLikes;
    @Bind(R.id.layout_carga)
    RelativeLayout layoutCarga;
    @Bind(R.id.layout_principal_visualizacion)
    RelativeLayout layoutPrincipalVisualizacion;
    Service service;
    @Bind(R.id.zoom_imagen)
    ImageView zoomImagen;

    FloatingActionButton edit_receta;
    FloatingActionButton delete_receta;
    ArrayList<LineaIngredienteVisualizacion> lista_lineas_ingrdientes;

    ArrayList<LineaPasoVisualizacion> lista_lineas_pasos;

    ConectionRetrofit conectionRetrofit;

    View rootView;
    static int id_recepta;
    Recepta recepta;
    Seguidor seguidor;
    Respuesta respuesta;
    Usuari usuario;

    Like like_receta;
    MainActivity mainActivity;
    static boolean receta_propia;
    static SharedPreferences prefe;
    AlphaAnimation alpha;

    public static FragmentVisualizacionReceta newInstance(int idReceta, boolean visualizacion_receta_propia, SharedPreferences sharedPreferences) {
        id_recepta = idReceta;
        receta_propia = visualizacion_receta_propia;
        prefe = sharedPreferences;
        FragmentVisualizacionReceta fragment_visualizacion_receta = new FragmentVisualizacionReceta();
        return fragment_visualizacion_receta;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (receta_propia) {
            rootView = inflater.inflate(R.layout.visualizacion_receta_propia, container, false);
        } else {
            rootView = inflater.inflate(R.layout.visualizacion_receta, container, false);
        }

        mainActivity = (MainActivity) getActivity();
        conectionRetrofit = new ConectionRetrofit();
        service = conectionRetrofit.getService();

        ButterKnife.bind(this, rootView);
        carga_elementos_layout();
        selectReceta(id_recepta);
        controlFloatingButton();

        alpha = new AlphaAnimation(0.5F, 0.5F);
        alpha.setDuration(0);
        alpha.setFillAfter(true);

        zoomImagen.setFocusable(true);
        zoomImagen.setFocusableInTouchMode(true);

        return rootView;
    }

    /**
     * En caso de estar visualizando una receta propia hace el inflado de los botones editar y eliminar
     */
    public void controlFloatingButton() {

        if (receta_propia) {
            edit_receta = (FloatingActionButton) rootView.findViewById(R.id.edit_receta);
            edit_receta.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mainActivity.cambia_fragment(FragmentInsercionReceta.newInstance(true, recepta, prefe));
                }
            });
            delete_receta = (FloatingActionButton) rootView.findViewById(R.id.delete_receta);
            delete_receta.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    verifica_eliminacion();
                }
            });
        }
    }

    /**
     * Carga en listas elemntos del layout para su manipulación
     */
    public void carga_elementos_layout() {
        lista_lineas_ingrdientes = new ArrayList<>();

        lista_lineas_ingrdientes.add(new LineaIngredienteVisualizacion(nombreIngrediente1, cantidadIngrediente1, tipoCantidadIngrediente1));
        lista_lineas_ingrdientes.add(new LineaIngredienteVisualizacion(nombreIngrediente2, cantidadIngrediente2, tipoCantidadIngrediente2));
        lista_lineas_ingrdientes.add(new LineaIngredienteVisualizacion(nombreIngrediente3, cantidadIngrediente3, tipoCantidadIngrediente3));
        lista_lineas_ingrdientes.add(new LineaIngredienteVisualizacion(nombreIngrediente4, cantidadIngrediente4, tipoCantidadIngrediente4));
        lista_lineas_ingrdientes.add(new LineaIngredienteVisualizacion(nombreIngrediente5, cantidadIngrediente5, tipoCantidadIngrediente5));
        lista_lineas_ingrdientes.add(new LineaIngredienteVisualizacion(nombreIngrediente6, cantidadIngrediente6, tipoCantidadIngrediente6));

        lista_lineas_pasos = new ArrayList<>();
        lista_lineas_pasos.add(new LineaPasoVisualizacion(descripcionPaso1, imagenPaso1));
        lista_lineas_pasos.add(new LineaPasoVisualizacion(descripcionPaso2, imagenPaso2, lineaPaso12));
        lista_lineas_pasos.add(new LineaPasoVisualizacion(descripcionPaso3, imagenPaso3, lineaPaso23));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * Realiza petición al servidor de la información de una receta
     *
     * @param id_receta
     */
    public void selectReceta(int id_receta) {
        Gson gson = new Gson();
        recepta = new Recepta(id_receta);
        String jsonReceta = gson.toJson(recepta);
        RequestBody requestReceta = RequestBody.create(MediaType.parse("text/plain"), jsonReceta);

        Action action = new Action("selectReceptaAjena");
        String jsonAction = gson.toJson(action);
        RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

        usuario = new Usuari();
        usuario.setId_usuari(Integer.parseInt(prefe.getString("id_usuario", "")));
        String jsonIdUsuari = gson.toJson(usuario);
        RequestBody requestIdUsuari = RequestBody.create(MediaType.parse("text/plain"), jsonIdUsuari);

        Call<Recepta> call = service.select_receta(requestAction, requestReceta, requestIdUsuari);
        call.enqueue(new Callback<Recepta>() {
            @Override
            public void onResponse(Response<Recepta> response) {
                recepta = response.body();
                if (rootView != null) {
                    layoutCarga.setVisibility(View.GONE);
                    if (receta_propia) {
                        edit_receta.setVisibility(View.VISIBLE);
                        delete_receta.setVisibility(View.VISIBLE);
                    }
                    layoutPrincipalVisualizacion.setVisibility(View.VISIBLE);
                    cargaReceta();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }

    /**
     * Realiza la petición de eliminación de la receta
     *
     * @param service
     * @param id_receta
     */
    public void delete_receta(Service service, int id_receta) {
        Gson gson = new Gson();
        Recepta delete_receta = new Recepta(id_receta);
        String jsonReceta = gson.toJson(delete_receta);
        RequestBody requestReceta = RequestBody.create(MediaType.parse("text/plain"), jsonReceta);

        Action action = new Action("deleteRecepta");
        String jsonAction = gson.toJson(action);
        RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

        Call<Respuesta> call = service.delete_receta(requestAction, requestReceta);
        call.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Response<Respuesta> response) {
                respuesta = response.body();
                if (respuesta.isRespuesta()) {
                    Toast.makeText(getContext(), "Receta eliminada", Toast.LENGTH_LONG).show();
                    mainActivity.onBackPressed();
                } else {
                    Toast.makeText(getContext(), "No se ha podido realizar la operación.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                Log.e("delete", "onFailure", t);
            }
        });
    }

    /**
     * Carga información receta enviada por el servidor a la vista
     */
    public void cargaReceta() {
        titolRecepta.setText(recepta.getTitol_recepta());
        byte[] imagen_receta = recepta.getImatge_recepta();
        Bitmap bitmap_imagen = BitmapFactory.decodeByteArray(imagen_receta, 0, imagen_receta.length);
        imagenReceta.setImageBitmap(bitmap_imagen);
        numComensales.setText(recepta.getNumero_comensals().toString());
        categoriaMenu.setText(recepta.getCategoria_menu());
        nombreCreador.setText(recepta.getNomPropietari());
        contadorLikes.setText(recepta.getContador_likes().toString());
        if (recepta.isFollow()) {
            star.setChecked(true);
        }
        if (recepta.isLike()) {
            like.setChecked(true);
        }
        carga_ingredientes();
        carga_pasos();
        if (recepta.getId_usuari() == Integer.parseInt(prefe.getString("id_usuario", ""))) {
            star.setVisibility(View.INVISIBLE);
            like.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Realiza resize escalado por width
     *
     * @param b
     * @param width
     * @return
     */
    public static Bitmap scaleToFitWidth(Bitmap b, int width) {
        float factor = width / (float) b.getWidth();
        return Bitmap.createScaledBitmap(b, width, (int) (b.getHeight() * factor), true);
    }

    /**
     * Carga información ingredientes mínima respuesta servidor en la vista
     */
    public void carga_ingredientes() {
        ArrayList<Ingredient> ingredientes = recepta.getListIngredients();
        if (ingredientes.size() > 1) {
            nombreIngrediente1.setText(ingredientes.get(0).getNom_ingredient());
            cantidadIngrediente1.setText(ingredientes.get(0).getQuantitat().toString());
            tipoCantidadIngrediente1.setText(ingredientes.get(0).getTipus_quantitat());
            carga_ingredientes_extras(ingredientes);
        } else {
            nombreIngrediente1.setText(ingredientes.get(0).getNom_ingredient());
            cantidadIngrediente1.setText(ingredientes.get(0).getQuantitat().toString());
            tipoCantidadIngrediente1.setText(ingredientes.get(0).getTipus_quantitat());
        }
    }

    /*
    Carga información ingredientes extras respuesta servidor en la vista
     */
    public void carga_ingredientes_extras(ArrayList<Ingredient> ingredientes) {

        for (int i = 1; i < ingredientes.size(); i++) {
            Ingredient ingrediente_actual = ingredientes.get(i);
            LineaIngredienteVisualizacion linea_ingrediente = lista_lineas_ingrdientes.get(i);
            if (linea_ingrediente.getNombre_ingrediente().getVisibility() == View.GONE) {
                linea_ingrediente.getNombre_ingrediente().setVisibility(View.VISIBLE);
                linea_ingrediente.getCantidad().setVisibility(View.VISIBLE);
                linea_ingrediente.getTipo_cantidad().setVisibility(View.VISIBLE);
            }
            linea_ingrediente.getNombre_ingrediente().setText(ingrediente_actual.getNom_ingredient());
            linea_ingrediente.getCantidad().setText(ingrediente_actual.getQuantitat().toString());
            linea_ingrediente.getTipo_cantidad().setText(ingrediente_actual.getTipus_quantitat());
        }
    }

    /**
     * Carga información pasos mínima respuesta servidor en la vista
     */
    public void carga_pasos() {
        ArrayList<Pas> pasos = recepta.getListPassos();
        if (pasos.size() > 1) {
            descripcionPaso1.setText(pasos.get(0).getDescripcio_pas());
            byte[] imagen_paso = pasos.get(0).getImatge_pas();
            Bitmap bitmap_paso = BitmapFactory.decodeByteArray(imagen_paso, 0, imagen_paso.length);
            imagenPaso1.setImageBitmap(bitmap_paso);
            carga_pasos_extras(pasos);
        } else {
            descripcionPaso1.setText(pasos.get(0).getDescripcio_pas());
            byte[] imagen_paso = pasos.get(0).getImatge_pas();
            Bitmap bitmap_paso = BitmapFactory.decodeByteArray(imagen_paso, 0, imagen_paso.length);
            imagenPaso1.setImageBitmap(bitmap_paso);
        }
    }

    /**
     * Carga información pasos extras respuesta servidor en la vista
     *
     * @param pasos
     */
    public void carga_pasos_extras(ArrayList<Pas> pasos) {

        for (int i = 1; i < pasos.size(); i++) {
            Pas paso_actual = pasos.get(i);
            LineaPasoVisualizacion linea_paso = lista_lineas_pasos.get(i);
            if (linea_paso.getDescripcion_paso().getVisibility() == View.GONE) {
                linea_paso.getDescripcion_paso().setVisibility(View.VISIBLE);
                linea_paso.getImagen_paso().setVisibility(View.VISIBLE);
                linea_paso.getLinea_separacion().setVisibility(View.VISIBLE);
            }
            linea_paso.getDescripcion_paso().setText(paso_actual.getDescripcio_pas());
            byte[] imagen_paso = paso_actual.getImatge_pas();
            Bitmap bitmap_paso = BitmapFactory.decodeByteArray(imagen_paso, 0, imagen_paso.length);
            linea_paso.getImagen_paso().setImageBitmap(bitmap_paso);
        }
    }

    /**
     * Envía solicitud al servidor para seguir o dejar de seguir al creador de la receta
     *
     * @param service
     */
    public void modifica_seguidor(Service service) {
        Gson gson = new Gson();
        seguidor = new Seguidor(Integer.parseInt(prefe.getString("id_usuario", "")), recepta.getId_usuari());
        String jsonSeguidor = gson.toJson(seguidor);
        RequestBody requestSeguidor = RequestBody.create(MediaType.parse("text/plain"), jsonSeguidor);
        Action action;
        if (star.isChecked()) {
            action = new Action("insert");
        } else {
            action = new Action("delete");
        }
        String jsonAction = gson.toJson(action);
        RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

        Call<Respuesta> call = service.seguir_usuario(requestAction, requestSeguidor);
        call.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Response<Respuesta> response) {
                respuesta = response.body();
                if (respuesta.isRespuesta() && rootView != null) {
                    if (star.isChecked()) {
                        Toast.makeText(getContext(), "Ahora sigues a " + recepta.getNomPropietari(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), "Ahora no sigues a " + recepta.getNomPropietari(), Toast.LENGTH_LONG).show();
                    }
                } else if (rootView != null) {
                    if (star.isChecked()) {
                        star.setChecked(false);
                    } else {
                        star.setChecked(true);
                    }
                    Toast.makeText(getContext(), "No se ha podido realizar la operación", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                Log.e("MainActivity", "onFailure", t);
            }
        });
    }

    /**
     * Envía solicitud al servidor para otorgar o sacar like a la receta
     *
     * @param service
     */
    public void modifica_like(Service service) {
        Gson gson = new Gson();
        like_receta = new Like(Integer.parseInt(prefe.getString("id_usuario", "")), recepta.getId_recepta());
        String jsonLike = gson.toJson(like_receta);
        RequestBody requestLike = RequestBody.create(MediaType.parse("text/plain"), jsonLike);
        Action action;
        final String accion;
        if (like.isChecked()) {
            accion = "insert";
        } else {
            accion = "delete";
        }
        action = new Action(accion);
        String jsonAction = gson.toJson(action);
        RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

        Call<Respuesta> call = service.like_receta(requestAction, requestLike);
        call.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Response<Respuesta> response) {
                respuesta = response.body();
                if (respuesta.isRespuesta() && rootView != null) {
                    if (accion.equals("insert")) {
                        contadorLikes.setText(String.valueOf((Integer.parseInt(contadorLikes.getText().toString()) + 1)));
                    } else {
                        contadorLikes.setText(String.valueOf((Integer.parseInt(contadorLikes.getText().toString()) - 1)));
                    }
                } else if (rootView != null) {
                    if (like.isChecked()) {
                        like.setChecked(false);
                    } else {
                        like.setChecked(true);
                    }
                    Toast.makeText(getContext(), "No se ha podido realizar la operación", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                Log.e("MainActivity", "onFailure", t);
            }
        });
    }

    /**
     * Verifica con un ALertDialog la confirmación de  eliminación de la receta
     */
    public void verifica_eliminacion() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Eliminar receta");
        alert.setMessage("Estas seguro de eliminar esta receta?");
        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                delete_receta(conectionRetrofit.getService(), recepta.getId_recepta());
            }
        });
        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.show();
    }

    @Override
    public void onDetach() {
        mainActivity = null;
        rootView = null;
        super.onDetach();
    }

    @OnClick({R.id.imagen_receta, R.id.star, R.id.like, R.id.imagen_paso_1, R.id.imagen_paso_2, R.id.imagen_paso_3, R.id.layout_padre})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imagen_receta:
                layoutPrincipalVisualizacion.startAnimation(alpha);
                zoomImagen.setImageDrawable(imagenReceta.getDrawable());
                zoomImagen.setVisibility(View.VISIBLE);
                break;
            case R.id.star:
                modifica_seguidor(conectionRetrofit.getService());
                break;
            case R.id.like:
                modifica_like(conectionRetrofit.getService());
                break;
            case R.id.imagen_paso_1:
                layoutPrincipalVisualizacion.startAnimation(alpha);
                zoomImagen.setImageDrawable(imagenPaso1.getDrawable());
                zoomImagen.setVisibility(View.VISIBLE);
                break;
            case R.id.imagen_paso_2:
                layoutPrincipalVisualizacion.startAnimation(alpha);
                zoomImagen.setImageDrawable(imagenPaso2.getDrawable());
                zoomImagen.setVisibility(View.VISIBLE);
                break;
            case R.id.imagen_paso_3:
                layoutPrincipalVisualizacion.startAnimation(alpha);
                zoomImagen.setImageDrawable(imagenPaso3.getDrawable());
                zoomImagen.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_padre:
                if (zoomImagen.getVisibility() == View.VISIBLE) {
                    zoomImagen.setVisibility(View.INVISIBLE);
                    layoutPrincipalVisualizacion.clearAnimation();
                }
                break;
        }
        if (view.getId() != R.id.layout_padre) {
            zoomImagen.requestFocus();
        }
    }
}
