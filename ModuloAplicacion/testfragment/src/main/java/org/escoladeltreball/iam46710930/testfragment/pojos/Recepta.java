package org.escoladeltreball.iam46710930.testfragment.pojos;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class Recepta  implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id_recepta;
	private Integer id_usuari;
	private String titol_recepta;
	private Integer contador_likes;
	private Integer numero_comensals;
	private Date data_creacio_recepta;
	private String categoria_menu;
	private byte[] imatge_recepta;
	private ArrayList<Pas> listPassos;
	private ArrayList<Ingredient> listIngredients;
	private boolean like;
	private boolean follow;
	private String nomPropietari;

	public Recepta() {}

	public Recepta(Integer id_recepta) {
		this.id_recepta=id_recepta;
	}

	public Integer getId_recepta() {
		return id_recepta;
	}

	public void setId_recepta(Integer id_recepta) {
		this.id_recepta = id_recepta;
	}

	public Integer getId_usuari() {
		return id_usuari;
	}

	public void setId_usuari(Integer id_usuari) {
		this.id_usuari = id_usuari;
	}

	public String getTitol_recepta() {
		return titol_recepta;
	}

	public void setTitol_recepta(String titol_recepta) {
		this.titol_recepta = titol_recepta;
	}

	public Integer getContador_likes() {
		return contador_likes;
	}

	public void setContador_likes(Integer contador_likes) {
		this.contador_likes = contador_likes;
	}

	public Date getData_creacio_recepta() {
		return data_creacio_recepta;
	}

	public void setData_creacio_recepta(Date date) {
		this.data_creacio_recepta = date;
	}

	public byte[] getImatge_recepta() {
		return imatge_recepta;
	}

	public void setImatge_recepta(byte[] imatge_recepta) {
		this.imatge_recepta = imatge_recepta;
	}

	public Integer getNumero_comensals() {
		return numero_comensals;
	}

	public void setNumero_comensals(Integer numero_comensals) {
		this.numero_comensals = numero_comensals;
	}

	public ArrayList<Pas> getListPassos() {
		return listPassos;
	}

	public void setListPassos(ArrayList<Pas> listPassos) {
		this.listPassos = listPassos;
	}

	public ArrayList<Ingredient> getListIngredients() {
		return listIngredients;
	}

	public void setListIngredients(ArrayList<Ingredient> listIngredients) {
		this.listIngredients = listIngredients;
	}

	public String getCategoria_menu() {
		return categoria_menu;
	}

	public void setCategoria_menu(String categoria_menu) {
		this.categoria_menu = categoria_menu;
	}

	public boolean isFollow() {
		return follow;
	}

	public void setFollow(boolean follow) {
		this.follow = follow;
	}

	public boolean isLike() {
		return like;
	}

	public void setLike(boolean like) {
		this.like = like;
	}

	public String getNomPropietari() {
		return nomPropietari;
	}

	public void setNomPropietari(String nomPropietari) {
		this.nomPropietari = nomPropietari;
	}


	@Override
	public String toString() {
		return "Recepta [id_recepta=" + id_recepta + ", id_usuari=" + id_usuari + ", titol_recepta=" + titol_recepta
				+ ", contador_likes=" + contador_likes + ", data_creacio_recepta=" + data_creacio_recepta
				+ ", categoria_menu_recepta=" + categoria_menu + ", imatge_recepta="
				+ Arrays.toString(imatge_recepta) + "]";
	}



}