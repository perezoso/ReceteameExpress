package org.escoladeltreball.iam46710930.testfragment.fragments;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.activities.MainActivity;
import org.escoladeltreball.iam46710930.testfragment.pojos.Action;
import org.escoladeltreball.iam46710930.testfragment.pojos.Respuesta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;
import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class FragmentEditarPerfil extends Fragment {

    ConectionRetrofit conectionRetrofit;
    Service service;
    View rootView;
    MainActivity mainActivity;
    static SharedPreferences prefe;

    @Bind(R.id.nombre_usuario_perfil)
    EditText nombreUsuarioPerfil;
    @Bind(R.id.mail_usuario_perfil)
    EditText mailUsuarioPerfil;
    @Bind(R.id.nuevo_pass)
    EditText nuevoPass;
    @Bind(R.id.repetecion_nuevo_pass)
    EditText repetecionNuevoPass;

    Usuari usuario;

    public static FragmentEditarPerfil newInstance(SharedPreferences sharedPreferences) {
        FragmentEditarPerfil fragment_editar_perfil = new FragmentEditarPerfil();
        prefe = sharedPreferences;
        return fragment_editar_perfil;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.editar_perfil, container, false);
        conectionRetrofit = new ConectionRetrofit();
        service = conectionRetrofit.getService();
        mainActivity = (MainActivity) getActivity();
        ButterKnife.bind(this, rootView);
        carga_info();
        return rootView;
    }

    /**
     * Carga información del usuario ubicada en sharedPreferences
     */
    public void carga_info() {
        nombreUsuarioPerfil.setText(prefe.getString("nombre_usuario", ""));
        mailUsuarioPerfil.setText(prefe.getString("mail_usuario", ""));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * Comprueba campos layout previo a su envío
     *
     * @return
     */
    public boolean check_campos() {
        String campo = nombreUsuarioPerfil.getText().toString().trim();
        if (campo.equals("")) {
            nombreUsuarioPerfil.setError("campo obligatorio");
            return false;
        }
        usuario = new Usuari();
        usuario.setNom_usuari(campo);
        if ((campo = mailUsuarioPerfil.getText().toString().trim()).equals("")) {
            mailUsuarioPerfil.setError("campo obligatorio");
            return false;
        } else if (!campo.matches(".+@.+\\..+")) {
            mailUsuarioPerfil.setError("mail incorrecto");
            return false;
        }
        usuario.setMail_usuari(campo);
        campo = nuevoPass.getText().toString().trim();
        String repeticion_pass = repetecionNuevoPass.getText().toString().trim();
        if (!campo.equals("") || !repeticion_pass.equals("")) {
            if (!campo.equals(repeticion_pass)) {
                nuevoPass.setError("borre o ponga la misma contraseña");
                repetecionNuevoPass.setError("borre o ponga la misma contraseña");
                return false;
            }
        }
        usuario.setContrasenya_usuari(campo);
        usuario.setToken(prefe.getString("token", ""));
        return true;
    }

    /**
     * Envia la información al servidor para su actualización
     *
     * @param usuario_pass
     */
    public void editar_usuario(Usuari usuario_pass) {
        Gson gson = new Gson();
        String jsonUsuarioPass = gson.toJson(usuario_pass);
        RequestBody requestUsuarioPass = RequestBody.create(MediaType.parse("text/plain"), jsonUsuarioPass);

        String jsonUsuario = gson.toJson(usuario);
        RequestBody requestUsuario = RequestBody.create(MediaType.parse("text/plain"), jsonUsuario);

        Action action = new Action("updateUsuario");
        String jsonAction = gson.toJson(action);
        RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

        Call<Respuesta> call = service.editar_usuario(requestAction, requestUsuarioPass, requestUsuario);
        call.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Response<Respuesta> response) {
                Respuesta respuesta = response.body();
                if (respuesta.isRespuesta() && rootView != null) {
                    guardaDatos();
                    Toast.makeText(mainActivity, "cambios realizados", Toast.LENGTH_LONG).show();
                    mainActivity.onBackPressed();
                } else {
                    int error = respuesta.getRespuestaControl();
                    if (error == 0) {
                        Toast.makeText(mainActivity, "contraseña incorrecta", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mainActivity, "nombre o mail ya existentes", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                Log.e("delete", "onFailure", t);
            }
        });

    }

    /**
     * Guarda los cambios realizados en la información de perfil
     */
    public void guardaDatos() {
        SharedPreferences.Editor editor = prefe.edit();
        editor.putString("nombre_usuario", usuario.getNom_usuari());
        editor.putString("mail_usuario", usuario.getMail_usuari());
        editor.apply();
    }

    @OnClick(R.id.button_modif_perfil)
    public void onClick() {
        if (check_campos()) {
            verifica_pass();
        }
    }

    /**
     * Verifica  con un alertDialog la contraseña existente para su seguridad
     */
    public void verifica_pass() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Contraseña");
        alert.setMessage("Introduce la contraseña actual:");
        final EditText input = new EditText(getContext());
        input.setHeight(120);
        input.setWidth(200);
        input.setGravity(Gravity.CENTER);
        input.setTransformationMethod(PasswordTransformationMethod.getInstance());
        input.setImeOptions(EditorInfo.IME_ACTION_DONE);
        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String pass = input.getText().toString().trim();
                Usuari usuarioPass = new Usuari();
                usuarioPass.setContrasenya_usuari(pass);
                usuarioPass.setToken(prefe.getString("token", ""));
                editar_usuario(usuarioPass);
            }
        });
        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.setView(input);
        alert.show();
    }

    @Override
    public void onDetach() {
        mainActivity = null;
        rootView = null;
        super.onDetach();
    }
}
