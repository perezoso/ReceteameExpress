package org.escoladeltreball.iam46710930.testfragment.fragments;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.activities.MainActivity;
import org.escoladeltreball.iam46710930.testfragment.adapters.AdapterListadoRecetas;
import org.escoladeltreball.iam46710930.testfragment.pojos.Filtres;
import org.escoladeltreball.iam46710930.testfragment.pojos.Recepta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;
import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;

import java.util.ArrayList;

import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class FragmentMisRecetas extends Fragment implements View.OnClickListener {

    ConectionRetrofit conectionRetrofit;
    RecyclerView recyclerView;
    AdapterListadoRecetas adapter_listado_recetas;
    SearchView search;
    ArrayList<Recepta> receptes;
    View rootView;
    MainActivity mainActivity;
    Service service;
    FloatingActionButton addReceta;
    static SharedPreferences prefe;
    static int id_usuarioConvertSeguidor = 0;
    SearchView.OnQueryTextListener listener;

    public static FragmentMisRecetas newInstance(SharedPreferences sharedPreferences, int is_usuario_seguido_to_mis_recetas) {
        FragmentMisRecetas fragment_mis_recetas = new FragmentMisRecetas();
        prefe = sharedPreferences;
        id_usuarioConvertSeguidor = is_usuario_seguido_to_mis_recetas;
        return fragment_mis_recetas;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.listado_mis_recetas, container, false);

        conectionRetrofit = new ConectionRetrofit();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.list_recetas);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);

        service = conectionRetrofit.getService();

        selectFiltres();

        mainActivity = (MainActivity) getActivity();

        addReceta = (FloatingActionButton) rootView.findViewById(R.id.add_receta);
        if (id_usuarioConvertSeguidor == 0) {

            addReceta.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mainActivity.cambia_fragment(FragmentInsercionReceta.newInstance(false, null, prefe));
                }
            });
        } else {
            addReceta.setVisibility(View.GONE);
        }
        inicializacion_search();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * Inicializacion SearchView
     */
    public void inicializacion_search() {
        listener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query) {
                query = query.toLowerCase();
                final ArrayList<Recepta> filteredList = new ArrayList<>();

                for (int i = 0; i < receptes.size(); i++) {
                    final String text = receptes.get(i).getTitol_recepta().toLowerCase();
                    if (text.contains(query)) {
                        filteredList.add(receptes.get(i));
                    }
                }
                adapter_listado_recetas = new AdapterListadoRecetas(filteredList, getContext(), FragmentMisRecetas.this);
                recyclerView.setAdapter(adapter_listado_recetas);
                adapter_listado_recetas.notifyDataSetChanged();
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        };
    }

    /**
     * Solicita listado recetas creadas por el usuario al servidor
     */
    public void selectFiltres() {

        Gson gson = new Gson();
        Usuari usuario = new Usuari();
        if (id_usuarioConvertSeguidor == 0) {
            usuario.setId_usuari(Integer.parseInt(prefe.getString("id_usuario", "")));
        } else {
            usuario.setId_usuari(id_usuarioConvertSeguidor);
        }
        String jsonUsuario = gson.toJson(usuario);
        RequestBody requestUsuario = RequestBody.create(MediaType.parse("text/plain"), jsonUsuario);
        Filtres filtro = new Filtres("Mis Recetas");
        String jsonFiltro = gson.toJson(filtro);
        RequestBody requestFiltro = RequestBody.create(MediaType.parse("text/plain"), jsonFiltro);
        Call<ArrayList<Recepta>> call = service.mis_recetas(requestFiltro, requestUsuario);
        call.enqueue(new Callback<ArrayList<Recepta>>() {
            @Override
            public void onResponse(Response<ArrayList<Recepta>> response) {

                receptes = response.body();
                if (rootView != null) {
                    recarga();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(mainActivity, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                Log.e("MainActivity", "onFailure", t);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void recarga() {
        adapter_listado_recetas = new AdapterListadoRecetas(receptes, getContext(), this);
        recyclerView.setAdapter(adapter_listado_recetas);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.getAdapter().notifyDataSetChanged();
        search = (SearchView) rootView.findViewById(R.id.buscador_listas);
        search.setOnQueryTextListener(listener);
    }

    @Override
    public void onClick(View v) {
        int id_recepta = adapter_listado_recetas.getArrayRecetas().get((Integer) v.getTag()).getId_recepta();
        Fragment newFragment;
        if (id_usuarioConvertSeguidor == 0) {
            newFragment = FragmentVisualizacionReceta.newInstance(id_recepta, true, prefe);
        } else {
            newFragment = FragmentVisualizacionReceta.newInstance(id_recepta, false, prefe);
        }
        mainActivity.cambia_fragment(newFragment);
    }

    @Override
    public void onDetach() {
        mainActivity = null;
        rootView = null;
        super.onDetach();
    }
}