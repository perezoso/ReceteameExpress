package org.escoladeltreball.iam46710930.testfragment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.pojos.Recepta;

import java.util.ArrayList;
import java.util.List;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class AdapterListadoRecetas extends RecyclerView.Adapter<AdapterListadoRecetas.RecetasViewHolder> {

    private final Context context;
    private List<Recepta> items = new ArrayList<>();
    View.OnClickListener onClickListener;

    public AdapterListadoRecetas(ArrayList<Recepta> receptes, Context context, View.OnClickListener onClickListener) {
        this.items = receptes;
        this.context = context;
        this.onClickListener = onClickListener;
        if (items.size()==0){
            Recepta sinResultados = new Recepta();
            sinResultados.setTitol_recepta("Categoría sin recetas");
            items.add(sinResultados);
        }
    }

    @Override
    public RecetasViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.receta_lista, viewGroup, false);
        RecetasViewHolder recetasViewHolder = new RecetasViewHolder(v);
        return recetasViewHolder;
    }

    @Override
    public void onBindViewHolder(RecetasViewHolder holder, int position) {

        final Recepta recepta = items.get(position);
        holder.nom_recepta.setText(recepta.getTitol_recepta());
        if(recepta.getTitol_recepta().equals("Categoría sin recetas")){
            holder.imatge_recepta.setVisibility(View.GONE);
        }else {
            Picasso.with(context)
                    .load("http://receteame.herokuapp.com/SRCImagen?action=selectReceptaImg&idRep=" + recepta.getId_recepta())
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(holder.imatge_recepta);
            holder.relativeLayout_receta.setOnClickListener(onClickListener);
            holder.relativeLayout_receta.setTag(position);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class RecetasViewHolder extends RecyclerView.ViewHolder {

        TextView nom_recepta;
        ImageView imatge_recepta;
        RelativeLayout relativeLayout_receta;

        public RecetasViewHolder(View itemView) {
            super(itemView);
            nom_recepta = (TextView) itemView.findViewById(R.id.nombre_receta_lista);
            imatge_recepta = (ImageView) itemView.findViewById(R.id.imagen_receta_lista);
            relativeLayout_receta = (RelativeLayout) itemView.findViewById(R.id.relative_receta_lista);
        }
    }

    public List<Recepta> getArrayRecetas() {
        return items;
    }
}
