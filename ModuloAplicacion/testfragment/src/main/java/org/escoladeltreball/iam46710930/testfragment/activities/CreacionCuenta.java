package org.escoladeltreball.iam46710930.testfragment.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.escoladeltreball.iam46710930.testfragment.retrofit.ConectionRetrofit;
import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.retrofit.Service;
import org.escoladeltreball.iam46710930.testfragment.pojos.Action;
import org.escoladeltreball.iam46710930.testfragment.pojos.Respuesta;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class CreacionCuenta extends Activity {

    @Bind(R.id.nombre_usuario_creacion)
    EditText nombreUsuarioCreacion;
    @Bind(R.id.mail_usuario_creacion)
    EditText mailUsuarioCreacion;
    @Bind(R.id.pass_usuario_creacion)
    EditText passUsuarioCreacion;
    @Bind(R.id.repeat_pass_usuario_creacion)
    EditText repeatPassUsuarioCreacion;

    ConectionRetrofit conectionRetrofit;
    Service service;

    ArrayList<EditText> elementos_cuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.creacion_cuenta);
        ButterKnife.bind(this);

        conectionRetrofit = new ConectionRetrofit();

        service = conectionRetrofit.getService();

        carga_elementos();
    }

    /**
     * Envia información usuario al servidor para su creción
     *
     * @param usuario
     */
    public void crea_usuario(Usuari usuario) {
        Gson gson = new Gson();

        if (usuario != null) {

            String jsonUsuario = gson.toJson(usuario);

            RequestBody requestUsuario = RequestBody.create(MediaType.parse("text/plain"), jsonUsuario);

            Action action = new Action("crearUsuario");
            String jsonAction = gson.toJson(action);
            RequestBody requestAction = RequestBody.create(MediaType.parse("text/plain"), jsonAction);

            Call<Respuesta> call = service.crea_usuario(requestAction, requestUsuario);
            call.enqueue(new Callback<Respuesta>() {
                @Override
                public void onResponse(Response<Respuesta> response) {

                    Respuesta respuesta = response.body();
                    if (respuesta.isRespuesta()) {
                        Toast.makeText(CreacionCuenta.this, "Usuario creado.", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(CreacionCuenta.this, "Usuario o mail ya existentes.", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Toast.makeText(CreacionCuenta.this, "Fallo de conexión.", Toast.LENGTH_LONG).show();
                    t.printStackTrace();
                    Log.e("delete", "onFailure", t);
                }
            });
        }
    }

    @OnClick(R.id.button_crear_cuenta)
    public void onClick() {
        crea_usuario(check_campos());

    }

    /**
     * Comprueba campos obligatórios
     *
     * @return Usuari
     */
    public Usuari check_campos() {

        for (EditText campo : elementos_cuenta) {
            if (campo.getText().toString().trim().equals("")) {
                campo.setError("campo obligatorio");
                return null;
            }
        }

        if (passUsuarioCreacion.getText().toString().trim().length() <= 6) {
            passUsuarioCreacion.setError("mínimo 7 letras/núm.");
            return null;
        }

        if (!passUsuarioCreacion.getText().toString().trim().equals(repeatPassUsuarioCreacion.getText().toString().trim())) {
            repeatPassUsuarioCreacion.setError("contrasenyas diferentes");
            return null;
        }

        if (!mailUsuarioCreacion.getText().toString().matches(".+@.+\\..+")) {
            mailUsuarioCreacion.setError("mail incorrecto");
            return null;
        }

        Usuari usuario = new Usuari();
        usuario.setNom_usuari(nombreUsuarioCreacion.getText().toString().trim());
        usuario.setContrasenya_usuari(passUsuarioCreacion.getText().toString().trim());
        usuario.setMail_usuari(mailUsuarioCreacion.getText().toString().trim());

        return usuario;
    }

    /**
     * Carga elementos layout en una lista para su comprobación
     */
    public void carga_elementos() {
        elementos_cuenta = new ArrayList<>();
        elementos_cuenta.add(nombreUsuarioCreacion);
        elementos_cuenta.add(mailUsuarioCreacion);
        elementos_cuenta.add(passUsuarioCreacion);
        elementos_cuenta.add(repeatPassUsuarioCreacion);
    }
}
