package org.escoladeltreball.iam46710930.testfragment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.escoladeltreball.iam46710930.testfragment.R;
import org.escoladeltreball.iam46710930.testfragment.pojos.Usuari;

import java.util.ArrayList;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class AdapterListadoUsuarios extends RecyclerView.Adapter<AdapterListadoUsuarios.UsuariosViewHolder> {


    private final Context context;
    private ArrayList<Usuari> items = new ArrayList<>();
    View.OnClickListener onClickListener;

    public AdapterListadoUsuarios(ArrayList<Usuari> receptes, Context context, View.OnClickListener onClickListener) {
        this.items = receptes;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @Override
    public UsuariosViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.seguidor_lista, viewGroup, false);
        UsuariosViewHolder pvh = new UsuariosViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(UsuariosViewHolder holder, int position) {
        final Usuari usuario = items.get(position);
        String nombreUsuario = usuario.getNom_usuari();
        char primero = nombreUsuario.charAt(0);
        nombreUsuario = Character.toUpperCase(primero) + nombreUsuario.substring(1, nombreUsuario.length());
        holder.nombre_usuario.setText(nombreUsuario);
        holder.ver_recetas_usuario.setOnClickListener(onClickListener);
        holder.star.setOnClickListener(onClickListener);
        holder.relativeSeguidorLista.setOnClickListener(onClickListener);
        if (!usuario.isFollow()) {
            holder.star.setChecked(false);
        } else {
            holder.star.setChecked(true);
        }
        holder.relativeSeguidorLista.setTag(position + "-ver_recetas");
        holder.ver_recetas_usuario.setTag(position + "-ver_recetas");
        holder.star.setTag(position + "-star");
        holder.cantidad_recetas.setText(usuario.getCantidadReceptas().toString());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class UsuariosViewHolder extends RecyclerView.ViewHolder {

        TextView nombre_usuario;
        ImageButton ver_recetas_usuario;
        CheckBox star;
        RelativeLayout relativeSeguidorLista;
        TextView cantidad_recetas;

        public UsuariosViewHolder(View itemView) {
            super(itemView);
            nombre_usuario = (TextView) itemView.findViewById(R.id.nombre_seguidor_lista);
            ver_recetas_usuario = (ImageButton) itemView.findViewById(R.id.listado_recetas_seguidor);
            star = (CheckBox) itemView.findViewById(R.id.star);
            relativeSeguidorLista = (RelativeLayout) itemView.findViewById(R.id.relative_seguidor_lista);
            cantidad_recetas = (TextView) itemView.findViewById(R.id.cantidad_recetas);
        }
    }

    public ArrayList<Usuari> getArrayUsuarios() {
        return items;
    }
}
