package org.escoladeltreball.iam46710930.testfragment.pojos;

import android.widget.EditText;
import android.widget.Spinner;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class LineaIngrediente {

    private EditText nombre_ingrediente;

    private EditText cantidad_ingrediente;

    private Spinner tipo_cantidad;

    public LineaIngrediente(EditText nombre_ingrediente, EditText cantidad_ingrediente, Spinner tipo_cantidad) {
        this.nombre_ingrediente = nombre_ingrediente;
        this.cantidad_ingrediente = cantidad_ingrediente;
        this.tipo_cantidad = tipo_cantidad;
    }

    public EditText getNombre_ingrediente() {
        return nombre_ingrediente;
    }

    public void setNombre_ingrediente(EditText nombre_ingrediente) {
        this.nombre_ingrediente = nombre_ingrediente;
    }

    public EditText getCantidad_ingrediente() {
        return cantidad_ingrediente;
    }

    public void setCantidad_ingrediente(EditText cantidad_ingrediente) {
        this.cantidad_ingrediente = cantidad_ingrediente;
    }

    public Spinner getTipo_cantidad() {
        return tipo_cantidad;
    }

    public void setTipo_cantidad(Spinner tipo_cantidad) {
        this.tipo_cantidad = tipo_cantidad;
    }
}
