package com.receteame.JavaPojos;

import java.io.Serializable;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class Like  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id_usuari;
	private Integer id_recepta;
	
	public Like () {}

	public Like (Integer id_usuari, Integer id_recepta) {
		this.id_usuari = id_usuari;
		this.id_recepta = id_recepta;
	}

	public Integer getId_usuari() {
		return id_usuari;
	}

	public void setId_usuari(Integer id_usuari) {
		this.id_usuari = id_usuari;
	}

	public Integer getId_recepta() {
		return id_recepta;
	}

	public void setId_recepta(Integer id_recepta) {
		this.id_recepta = id_recepta;
	}
	
	
}
