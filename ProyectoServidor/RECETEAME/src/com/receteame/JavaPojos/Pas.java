package com.receteame.JavaPojos;

import java.io.Serializable;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class Pas  implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id_recepta;
	private Integer id_pas;
	private String descripcio_pas;
	private byte [] imatge_pas;

	public Pas () {}
	
	/**
	 * SELECT
	 * 
	 * @param id_recepta
	 * @param id_pas
	 * @param descripcio_pas
	 * @param imatge_pas
	 */
	public Pas(Integer id_recepta, Integer id_pas, String descripcio_pas, byte[] imatge_pas) {
		this.id_recepta = id_recepta;
		this.id_pas = id_pas;
		this.descripcio_pas = descripcio_pas;
		this.imatge_pas = imatge_pas;
	}
	
	/**
	 * INSERT 
	 * 
	 * @param descripcio_pas
	 * @param imatge_pas
	 */
	public Pas(String descripcio_pas, byte[] imatge_pas) {
		this.descripcio_pas = descripcio_pas;
		this.imatge_pas = imatge_pas;
	}
	
	public Integer getId_recepta() {
		return id_recepta;
	}
	
	public void setId_recepta(Integer id_recepta) {
		this.id_recepta = id_recepta;
	}
	
	public Integer getId_pas() {
		return id_pas;
	}
	
	public void setId_pas(Integer id_pas) {
		this.id_pas = id_pas;
	}
	
	public String getDescripcio_pas() {
		return descripcio_pas;
	}
	
	public void setDescripcio_pas(String descripcio_pas) {
		this.descripcio_pas = descripcio_pas;
	}
	
	public byte[] getImatge_pas() {
		return imatge_pas;
	}
	
	public void setImatge_pas(byte[] imatge_pas) {
		this.imatge_pas = imatge_pas;
	}
	
}
