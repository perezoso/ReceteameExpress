package com.receteame.JavaPojos;

import java.io.Serializable;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class Filtres implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String filtro;
	
	public Filtres (String filtro) {
		this.filtro=filtro;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

}
