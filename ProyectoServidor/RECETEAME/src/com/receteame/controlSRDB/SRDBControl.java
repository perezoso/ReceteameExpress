package com.receteame.controlSRDB;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.dbcp2.BasicDataSource;

import com.google.gson.Gson;
import com.receteame.JavaPojos.Action;
import com.receteame.JavaPojos.Filtres;
import com.receteame.JavaPojos.Like;
import com.receteame.JavaPojos.Recepta;
import com.receteame.JavaPojos.Respuesta;
import com.receteame.JavaPojos.RespuestaInteger;
import com.receteame.JavaPojos.Seguidor;
import com.receteame.JavaPojos.Usuari;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class SRDBControl {

	private static final String ENCTYPE = "UTF-8";
	public static MasterQueryManagement masterQueryManagement;

	public SRDBControl() {
		masterQueryManagement = new MasterQueryManagement(getConnection());
	}

	/**
	 * 
	 * Envia una Respuesta en JSON
	 * 
	 * @param respuesta
	 * @return String 
	 */
	public static synchronized String getAnswerRespuesta ( Respuesta respuesta ) {
		Gson gson = new Gson();
		return gson.toJson(respuesta);			
	}
	
	/**
	 * 
	 * Envia una Respuesta False en JSON
	 * 
	 * @param respuesta
	 * @return String 
	 */
	public static synchronized String getAnswerRespuestaFalse() {
		Respuesta respuesta = new Respuesta(false);
		Gson gson = new Gson();
		return gson.toJson(respuesta);			
	}
	
	/**
	 * 
	 * Envia una Respuesta en JSON
	 * 
	 * @param respuesta
	 * @return String 
	 */
	public static synchronized String getAnswerRespuestaInteger ( Integer resultSelectLikes ) {
		RespuestaInteger respuestaInteger = new RespuestaInteger((resultSelectLikes!=-1) ? resultSelectLikes : 0 );
		Gson gson = new Gson();
		return gson.toJson(respuestaInteger);			
	}
	
	/**
	 * 
	 * Envia una Recepta en JSON
	 * 
	 * @param respuesta
	 * @return String 
	 */
	public static synchronized String getAnswerRecepta ( Recepta recepta ) {
		Gson gson = new Gson();
		return gson.toJson(recepta);			
	}
	
	/**
	 * 
	 * Envia una List Recepta en JSON
	 * 
	 * @param respuesta
	 * @return String 
	 */
	public static synchronized String getAnswerListRecepta ( ArrayList<Recepta> list ) {
		
		ArrayList<Recepta> listRecepta = new ArrayList<>();
		listRecepta.addAll(list);
		
		Gson gson = new Gson();
		return gson.toJson(listRecepta);			
	}
	
	/**
	 * 
	 * Envia una List Recepta en JSON
	 * 
	 * @param respuesta
	 * @return String 
	 */
	public static synchronized String getAnswerListUsuari ( ArrayList<Usuari> list ) {
		
		ArrayList<Usuari> listUsuari = new ArrayList<>();
		listUsuari.addAll(list);
		
		Gson gson = new Gson();
		return gson.toJson(listUsuari);			
	}
	
	/**
	 * 
	 * Envia un Usuario en JSON
	 * 
	 * @param respuesta
	 * @return String 
	 */
	public static synchronized String getAnswerUsuario ( Usuari usuari ) {
		Gson gson = new Gson();
		return gson.toJson(usuari);			
	}
	
	/**
	 * 
	 * Obtención de la connexión con postgres hospedado en HEROKU
	 * 
	 * Required : org.postgresql.Driver
	 * 
	 * http://receteame.herokuapp.com/
	 * 
	 * URL :
	 * jdbc:postgresql://ec2-54-228-219-2.eu-west-1.compute.amazonaws.com:5432/
	 * d52b52il6pfq91?sslmode=require
	 * 
	 * @return Connection
	 */
	@SuppressWarnings("resource")
	public static synchronized Connection getConnection() {
		BasicDataSource basicDataSource = null;
		try {
			basicDataSource = new BasicDataSource();
			basicDataSource.setDriverClassName("org.postgresql.Driver");
			basicDataSource.setUrl(
					"jdbc:postgresql://ec2-54-228-219-2.eu-west-1.compute.amazonaws.com:5432/d52b52il6pfq91?sslmode=require");
			basicDataSource.setUsername("cxcoakikocqpqi");
			basicDataSource.setPassword("RywG8I8ZMUJeTYYgejZ8aeWanL");
			return basicDataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * Transformación de un objeto Action a JSON
	 * 
	 * @param inputStream
	 * @param classToConvert
	 * @return Action
	 */
	public static synchronized Action parseObjectAction(InputStream inputStream, Class<Action> classToConvert) {

		Gson gson = new Gson();
		Action action = null;

		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ENCTYPE))) {

			action = gson.fromJson(bufferedReader, classToConvert);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return action;

	}

	/**
	 * 
	 * Transformación de un objeto Seguidor a JSON
	 * 
	 * @param inputStream
	 * @param classToConvert
	 * @return Seguidor
	 */
	public static synchronized Seguidor parseObjectSeguidor(InputStream inputStream, Class<Seguidor> classToConvert) {

		Gson gson = new Gson();
		Seguidor seguidor = null;

		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ENCTYPE))) {

			seguidor = gson.fromJson(bufferedReader, classToConvert);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return seguidor;

	}

	/**
	 * 
	 * Transformación de un objeto Recepta a JSON
	 * 
	 * @param inputStream
	 * @param classToConvert
	 * @return
	 */
	public static synchronized Recepta parseObjectRecepta(InputStream inputStream, Class<Recepta> classToConvert) {

		Gson gson = new Gson();
		Recepta recepta = null;

		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ENCTYPE))) {

			recepta = gson.fromJson(bufferedReader, classToConvert);

		} catch (IOException io) {
			io.printStackTrace();
		}

		return recepta;

	}

	/**
	 * Transformación de un objeto Usuari a JSON
	 * 
	 * @param inputStream
	 * @param classToConvert
	 * @return
	 */
	public static synchronized Usuari parseObjectUsuari(InputStream inputStream, Class<Usuari> classToConvert) {

		Gson gson = new Gson();
		Usuari usuari = null;

		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ENCTYPE))) {

			usuari = gson.fromJson(bufferedReader, classToConvert);

		} catch (IOException io) {
			io.printStackTrace();
		}

		return usuari;

	}

	/**
	 * 
	 * Transformación de un objeto Filtres a JSON
	 * 
	 * @param inputStream
	 * @param classToConvert
	 * @return
	 */
	public static synchronized Filtres parseObjectFiltres(InputStream inputStream, Class<Filtres> classToConvert) {

		Gson gson = new Gson();
		Filtres filtres = null;

		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, ENCTYPE))) {

			filtres = gson.fromJson(bufferedReader, classToConvert);

		} catch (IOException io) {
			io.printStackTrace();
		}

		return filtres;

	}
	
	/**
	 * 
	 * Transformación de un objeto Likes a JSON
	 * 
	 * @param inputStream
	 * @param classToConvert
	 * @return Like
	 */
	public static synchronized Like parseObjectLike(InputStream inputStream, Class<Like> classToConvert) {
		
		Gson gson = new Gson();
		Like like = null;
		
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,ENCTYPE))){
			
			like = gson.fromJson(bufferedReader, classToConvert);	
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return like;	
	}

}
