package com.receteame.controlSRDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.postgresql.util.PSQLException;

import com.receteame.JavaPojos.Ingredient;
import com.receteame.JavaPojos.Pas;
import com.receteame.JavaPojos.Recepta;
import com.receteame.JavaPojos.Respuesta;
import com.receteame.JavaPojos.Usuari;
import com.receteame.interfaces.ActionTypes;
import com.receteame.interfaces.BDConsultas;
import com.receteame.interfaces.LoggerMessages;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public class MasterQueryManagement implements BDConsultas, ActionTypes, LoggerMessages, AutoCloseable {

	private Connection connection;

	/**
	 * @param connection
	 */
	public MasterQueryManagement(Connection connection) {
		this.connection = connection;
	}

	// ----------------------- INICIO METHODs RECEPTES

	/**
	 * 
	 * Inserción de una receta
	 * 
	 * @param recepta
	 * @return Integer
	 */
	public synchronized Respuesta insertRecepta(Recepta recepta) {

		PreparedStatement preparedStatement = null;

		// Declare & load Object
		Integer result = 0;
		Integer idUsuari = recepta.getId_usuari();
		Integer idRecepta = -1;
		String titolRecepta = recepta.getTitol_recepta();
		String categoriaMenu = recepta.getCategoria_menu();
		Integer numeroComensals = recepta.getNumero_comensals();
		byte[] imatgeRecepta = recepta.getImatge_recepta();
		ArrayList<Pas> listPassos = recepta.getListPassos();
		ArrayList<Ingredient> listIngredients = recepta.getListIngredients();

		try {

			preparedStatement = connection.prepareStatement(INSERT_RECEPTES + VAL + SEP + VAL + SEP + VAL + SEP + VAL
					+ SEP + VAL + PAR_CL + RETURNING + ID_RECEPTA);
			preparedStatement.setInt(1, idUsuari);
			preparedStatement.setString(2, titolRecepta);
			preparedStatement.setInt(3, numeroComensals);
			preparedStatement.setString(4, categoriaMenu);
			preparedStatement.setBytes(5, imatgeRecepta);

			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				idRecepta = resultSet.getInt(1);

				LOGGER.info(DATOS_RECETA);

				Integer count = 0;
				for (Pas pas : listPassos) {
					count++;
					result = insertPas(idRecepta, pas, count);
				}

				if (result != -1) {
					LOGGER.info(DATOS_PASOS);
				}

				count = 0;
				for (Ingredient ingredient : listIngredients) {
					count++;
					result = insertIngredient(idRecepta, ingredient, count);
				}

				if (result != -1) {
					LOGGER.info(DATOS_INGREDIENTES);
				}

			}

		} catch (SQLException sql) {
			LOGGER.info(INR);
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException sql) {
					LOGGER.info(PS);
					sql.printStackTrace();
				}
			}

		}
		
		Respuesta respuesta = new Respuesta((result!=-1) ? true : false ); 
		
		return respuesta;
	}

	/**
	 * 
	 * Inserción de un ingrediente
	 * 
	 * @param idRecepta
	 * @param ingredient
	 * @param count
	 * @return
	 */
	private Integer insertIngredient(Integer idRecepta, Ingredient ingredient, Integer count) {

		PreparedStatement preparedStatement = null;
		Integer result = -1;

		LOGGER.info(INSERTANDO_INGREDIENTES + idRecepta);
		
		String nomIngredient = ingredient.getNom_ingredient();
		Integer quantitat = ingredient.getQuantitat();
		String tipus_quantiat = ingredient.getTipus_quantitat();

		try {

			preparedStatement = connection.prepareStatement(
					INSERT_INGREDIENTS + VAL_OP + VAL + SEP + VAL + SEP + VAL + SEP + VAL + SEP + VAL + PAR_CL);
			preparedStatement.setInt(1, count);
			preparedStatement.setInt(2, idRecepta);
			preparedStatement.setString(3, nomIngredient);
			preparedStatement.setDouble(4, quantitat);
			preparedStatement.setString(5, tipus_quantiat);
			result = preparedStatement.executeUpdate();

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException sql) {
					LOGGER.info(PS);
					sql.printStackTrace();
				}
			}
		}

		return result;
	}

	/**
	 * 
	 * Inserción de un paso
	 * 
	 * @param idRecepta
	 * @param pas
	 * @param count
	 * @return
	 */
	private Integer insertPas(Integer idRecepta, Pas pas, Integer count) {

		PreparedStatement preparedStatement = null;
		Integer result = -1;

		LOGGER.info(INSERTANDO_PASOS+idRecepta);
		
		String descripcioPas = pas.getDescripcio_pas();
		byte[] imatgePas = pas.getImatge_pas();

		try {

			preparedStatement = connection
					.prepareStatement(INSERT_PASSOS + VAL + SEP + VAL + SEP + VAL + SEP + VAL + PAR_CL);
			preparedStatement.setInt(1, idRecepta);
			preparedStatement.setInt(2, count);
			preparedStatement.setString(3, descripcioPas);
			preparedStatement.setBytes(4, imatgePas);
			result = preparedStatement.executeUpdate();

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException sql) {
					LOGGER.info(PS);
					sql.printStackTrace();
				}
			}
		}

		return result;

	}

	/**
	 * 
	 * Seleccionar una receta por idReceta
	 * 
	 * @param recepta
	 * @return Recepta
	 */
	public synchronized Recepta selectRecepta(Recepta recepta) {

		Statement statement = null;
		ResultSet resultSet = null;
		Integer idRecepta = recepta.getId_recepta();

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_RECEPTES_BYRECEPTA + idRecepta);

			while (resultSet.next()) {
				recepta.setId_usuari(resultSet.getInt(2));
				recepta.setTitol_recepta(resultSet.getString(3));
				recepta.setNumero_comensals(resultSet.getInt(4));
				recepta.setData_creacio_recepta(resultSet.getDate(5));
				recepta.setCategoria_menu(resultSet.getString(6));
				recepta.setImatge_recepta(resultSet.getBytes(7));
				recepta.setListPassos(selectPassos(idRecepta));
				recepta.setListIngredients(selectIngredients(idRecepta));
				recepta.setContador_likes(selectCntLikesRecepta(idRecepta));
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return recepta;

	}

	/**
	 * 
	 * Seleccionar una receta ajena al usuario que realiza la petición
	 * 
	 * @param recepta
	 * @param idUsuari
	 * @return Recepta
	 */
	public synchronized Recepta selectReceptaAjena(Recepta recepta, Integer idUsuari) {

		Statement statement = null;
		ResultSet resultSet = null;
		Integer idRecepta = recepta.getId_recepta();

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_RECEPTES_BYRECEPTA + idRecepta);

			while (resultSet.next()) {
				Integer own = resultSet.getInt(3);
				recepta.setId_usuari(own);
				recepta.setNomPropietari(resultSet.getString(1));
				recepta.setTitol_recepta(resultSet.getString(4));
				recepta.setNumero_comensals(resultSet.getInt(5));
				recepta.setData_creacio_recepta(resultSet.getDate(6));
				recepta.setCategoria_menu(resultSet.getString(7));
				recepta.setImatge_recepta(resultSet.getBytes(8));
				recepta.setListPassos(selectPassos(idRecepta));
				recepta.setListIngredients(selectIngredients(idRecepta));
				recepta.setContador_likes(selectCntLikesRecepta(idRecepta));
				recepta.setLike(isLike(idUsuari, idRecepta));
				recepta.setFollow(isFollow(idUsuari, own));
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return recepta;
	}

	/**
	 * 
	 * Seleccionar una lista de pasos referente al idReceta
	 * 
	 * @param idRecepta
	 * @return ArrayList<Pas>
	 */
	private ArrayList<Pas> selectPassos(Integer idRecepta) {

		ArrayList<Pas> listPassos = new ArrayList<>();

		Statement statement = null;
		ResultSet resultSet = null;
		Integer idPas = null;
		String descripcioPas = null;
		byte[] imatgePas = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_PAS_BYRECEPTA + idRecepta);

			while (resultSet.next()) {

				idPas = resultSet.getInt(2);
				descripcioPas = resultSet.getString(3);
				imatgePas = resultSet.getBytes(4);

				Pas pas = new Pas(idRecepta, idPas, descripcioPas, imatgePas);
				listPassos.add(pas);
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return listPassos;

	}

	/**
	 * 
	 * Seleccionar una lista de ingredientes referente al idReceta
	 * 
	 * @param idRecepta
	 * @return ArrayList<Ingredient>
	 */
	private ArrayList<Ingredient> selectIngredients(Integer idRecepta) {

		ArrayList<Ingredient> listIngredient = new ArrayList<>();
		Statement statement = null;
		ResultSet resultSet = null;
		Integer idIngredient = null;
		String nomIngredient = null;
		Integer quantitat = null;
		String tipusQuantitat = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_INGREDIENT_BYRECEPTA + idRecepta);

			while (resultSet.next()) {
				idIngredient = resultSet.getInt(1);
				nomIngredient = resultSet.getString(3);
				quantitat = resultSet.getInt(4);
				tipusQuantitat = resultSet.getString(5);
				listIngredient.add(new Ingredient(idIngredient, quantitat, nomIngredient, quantitat, tipusQuantitat));
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return listIngredient;

	}

	/**
	 * 
	 * Elimina una receta por idRecepta
	 * 
	 * @param idRecepta
	 * @return Respuesta
	 */
	public synchronized Respuesta deleteRecepta(Integer idRecepta) {

		Respuesta isDelete = null;
		Statement statement = null;
		Integer result = 0;

		try {
			statement = connection.createStatement();
			result = statement.executeUpdate(DELETE_RECEPTES + idRecepta);
			isDelete = new Respuesta((result != 0) ? true : false);
			LOGGER.info(" [ MQM ]  || Delete ? " + isDelete.isRespuesta());

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return isDelete;
	}

	/**
	 * 
	 * Comprobación sí la todos los metodos para la actualización han sido
	 * exitosos.
	 * 
	 * @param boRecepta
	 * @param boPassos
	 * @param boIngredients
	 * @return Respuesta
	 */
	private Respuesta getBooleanValue(boolean boRecepta, boolean boPassos, boolean boIngredients) {
		return new Respuesta(boRecepta && boPassos && boIngredients);
	}

	/**
	 * 
	 * Actualizar una receta
	 * 
	 * @param recepta
	 * @return Respuesta
	 */
	public Respuesta updateRecepta(Recepta recepta) {

		Integer result = 0;
		Respuesta respuesta = null;

		// Load & declaration
		Integer idRecepta = recepta.getId_recepta();
		Integer idUsuari = recepta.getId_usuari();
		String titolRecepta = recepta.getTitol_recepta();
		Integer numComensals = recepta.getNumero_comensals();
		String categoriaMenu = recepta.getCategoria_menu();
		byte[] imatgeRecepta = recepta.getImatge_recepta();
		ArrayList<Pas> listPassos = recepta.getListPassos();
		ArrayList<Ingredient> listIngredients = recepta.getListIngredients();

		// Tools
		PreparedStatement preparedStatement = null;

		try {

			preparedStatement = connection.prepareStatement(QUERY_UPDATE_RECEPTA);
			preparedStatement.setString(1, titolRecepta);
			preparedStatement.setInt(2, numComensals);
			preparedStatement.setString(3, categoriaMenu);
			preparedStatement.setBytes(4, imatgeRecepta);
			preparedStatement.setInt(5, idRecepta);
			preparedStatement.setInt(6, idUsuari);
			result = preparedStatement.executeUpdate();

			boolean boRecepta = (result != 0) ? true : false;

			LOGGER.info(DATOS_RECETA_UPDT + boRecepta);

			boolean boPassos = updatePassos(listPassos);

			LOGGER.info(DATOS_PASOS_UPDT + boPassos);

			boolean boIngredients = updateIngredients(listIngredients);

			LOGGER.info(DATOS_INGREDIENTES_UPDT + boIngredients);

			respuesta = getBooleanValue(boRecepta, boPassos, boIngredients);

			LOGGER.info(DATOS_UPDT + respuesta.isRespuesta());

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException sql) {
					LOGGER.info(PS);
					sql.printStackTrace();
				}
			}
		}

		return respuesta;
	}

	/**
	 * Eliminar una lista de ingredientes
	 * 
	 * @param sizeList
	 * @param idRecepta
	 * @return boolean
	 */
	private boolean deleteIngredients(Integer sizeList, Integer idRecepta) {

		Statement statement = null;
		Integer result = 0;
		boolean resultBoolean = false;

		try {

			statement = connection.createStatement();
			String query = DELETE_INGREDIENT + idRecepta + AND + ID_INGREDIENT + GREATER + sizeList;
			result = statement.executeUpdate(query);
			resultBoolean = (result != 0) ? true : false;

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException state) {
					LOGGER.info(S);
					state.printStackTrace();
				}
			}
		}

		return resultBoolean;

	}

	/**
	 * 
	 * Actualizar una lista de ingredientes
	 * 
	 * @param listIngredients
	 * @return boolean
	 */
	private boolean updateIngredients(ArrayList<Ingredient> listIngredients) {

		PreparedStatement preparedStatement = null;
		Integer result = 0;
		boolean update = false;
		Integer idRecepta = null;
		Integer listSize = listIngredients.size();

		if (listSize > 0) {

			idRecepta = listIngredients.get(0).getId_recepta();
			deleteIngredients(listSize, idRecepta);

		}

		try {
			int countID = 1;
			for (Ingredient ingredient : listIngredients) {

				Integer idIngredient = countID;
				String nomIngredient = ingredient.getNom_ingredient();
				Integer quantitat = ingredient.getQuantitat();
				String tipusQuantitat = ingredient.getTipus_quantitat();

				try {
					preparedStatement = connection.prepareStatement(QUERY_UPDATE_INGREDIENTS);
					preparedStatement.setString(1, nomIngredient);
					preparedStatement.setInt(2, quantitat);
					preparedStatement.setString(3, tipusQuantitat);
					preparedStatement.setInt(4, idIngredient);
					preparedStatement.setInt(5, idRecepta);
					result = preparedStatement.executeUpdate();
					update = (result != 0) ? true : false;

					if (!update) {
						Statement statement = connection.createStatement();
						final String query = INSERT_INGREDIENTS + VAL_OP + idIngredient + SEP + idRecepta + SEP + SQUOTE
								+ nomIngredient + SQUOTE + SEP + quantitat + SEP + SQUOTE + tipusQuantitat + SQUOTE
								+ PAR_CL;
						int insertUpdate = statement.executeUpdate(query);
						statement.close();

						if (insertUpdate != 0) {
							update = true;
						}

						LOGGER.info(DATOS_UPDT + update);
					}
					countID++;

				} catch (SQLException sql) {
					LOGGER.info(SQL);
					sql.printStackTrace();
				}

			}

		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException sql) {
					LOGGER.info(PS);
					sql.printStackTrace();
				}
			}
		}

		return update;

	}

	/**
	 * 
	 * Eliminar una lista de pasos
	 * 
	 * @param sizeList
	 * @param idRecepta
	 * @return boolean
	 */
	private boolean deletePassos(Integer sizeList, Integer idRecepta) {

		Statement statement = null;
		Integer result = 0;
		boolean resultBoolean = false;

		try {

			statement = connection.createStatement();
			result = statement.executeUpdate(DELETE_PAS + idRecepta + AND_ID_PAS_G + sizeList);
			resultBoolean = (result != 0) ? true : false;

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return resultBoolean;

	}

	/**
	 * 
	 * Actualizar una lista de pasos
	 * 
	 * @param listPassos
	 * @return boolean
	 */
	private boolean updatePassos(ArrayList<Pas> listPassos) {

		PreparedStatement preparedStatement = null;
		Integer result = 0;
		boolean update = false;
		Integer idRecepta = null;
		Integer listSize = listPassos.size();

		if (listSize > 0) {

			idRecepta = listPassos.get(0).getId_recepta();
			deletePassos(listSize, idRecepta);

		}

		int countID = 1;
		for (Pas pas : listPassos) {

			Integer idPas = countID;
			String descripcioPas = pas.getDescripcio_pas();
			byte[] imatgePas = pas.getImatge_pas();

			try {

				preparedStatement = connection.prepareStatement(QUERY_UPDATE_PASSOS);
				preparedStatement.setString(1, descripcioPas);
				preparedStatement.setBytes(2, imatgePas);
				preparedStatement.setInt(3, idRecepta);
				preparedStatement.setInt(4, idPas);
				result = preparedStatement.executeUpdate();
				update = (result != 0) ? true : false;
				preparedStatement.close();

				if (!update) {

					preparedStatement = connection
							.prepareStatement(INSERT_PASSOS + VAL + SEP + VAL + SEP + VAL + SEP + VAL + PAR_CL);
					preparedStatement.setInt(1, idRecepta);
					preparedStatement.setInt(2, idPas);
					preparedStatement.setString(3, descripcioPas);
					preparedStatement.setBytes(4, imatgePas);
					result = preparedStatement.executeUpdate();

					if (result != 0) {
						update = true;
					}

					LOGGER.info("[ MQM ] @Update [ Passos ] " + update);

					preparedStatement.close();
				}

				countID++;

			} catch (SQLException sql) {
				LOGGER.info(SQL);
				sql.printStackTrace();
			} finally {
				if (preparedStatement != null) {
					try {
						preparedStatement.close();
					} catch (SQLException sql) {
						LOGGER.info(PS);
						sql.printStackTrace();
					}
				}
			}

		}

		return update;

	}

	/**
	 * 
	 * Seleccionar cantidad de recetas de un usuario por idUsuari
	 * 
	 * @param idUsuari
	 * @return Integer
	 */
	public synchronized Integer selectCntReceptesByUser(Integer idUsuari) {

		Integer cantidadLikes = 0;
		Statement statement = null;
		ResultSet resultSet = null;

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_CNT_RECEPTES + idUsuari);
			cantidadLikes = (resultSet.next()) ? resultSet.getInt(1) : cantidadLikes;

		} catch (PSQLException psql) {
			LOGGER.info(PSQL);
			psql.printStackTrace();
		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return cantidadLikes;

	}

	// ----------------------- FIN METHODs RECEPTES
	
	// ----------------------- INICIO METHODs LIKES

	/**
	 * Inserción Me gusta de un usuario a una receta
	 * @param idUsuari
	 * @param idRecepta
	 * @return boolean
	 */
	public synchronized Respuesta insertLikes(Integer idUsuari, Integer idRecepta) {

		Statement statement = null;
		Integer result = 0;
		boolean resultBoolean = false;

		try {

			statement = connection.createStatement();
			result = statement.executeUpdate(INSERT_LIKES + VAL_OP + idUsuari + SEP + idRecepta + PAR_CL);
			resultBoolean = (result != 0) ? true : false;

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		Respuesta respuesta = new Respuesta(resultBoolean);
		
		return respuesta;
	}

	/**
	 * 
	 * Seleccion de cantidad de likes en una receta por idReceta
	 * 
	 * @param idReceta
	 * @return Integer
	 */
	public synchronized Integer selectCntLikesRecepta(Integer idReceta) {

		Integer result = -1;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_CNT_LIKES_RECEPTA + idReceta);
			result = (resultSet.next()) ? resultSet.getInt(1) : result;
		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return result;

	}

	/**
	 * 
	 * Seleccionar cantidad total de likes por idUsuari
	 * 
	 * @param idUsuari
	 * @return int
	 */
	public int selectCntTotalLikes(Integer idUsuari) {

		Integer result = -1;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_CNT_TOTAL_LIKES + idUsuari);
			result = (resultSet.next()) ? resultSet.getInt(1) : result;
		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return result;

	}

	/**
	 * 
	 * Comprobar si una receta tiene Me gusta
	 * 
	 * @param idUsuari
	 * @param idRecepta
	 * @return boolean
	 */
	public boolean isLike(Integer idUsuari, Integer idRecepta) {

		boolean isLike = false;
		// PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT id_usuari, id_recepta FROM likes WHERE id_usuari = " + idUsuari
					+ " AND id_recepta = " + idRecepta);
			isLike = (resultSet.next()) ? true : false;

			LOGGER.info(" [ MQM ] ISLIKE : " + isLike);
			LOGGER.info(" [ MQM ] user : " + idUsuari);
			LOGGER.info(" [ MQM ] recepta : " + idRecepta);

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		}

		return isLike;
	}

	
	/**
	 * 
	 * Eliminar un Me gusta de una receta
	 * 
	 * @param idUsuari
	 * @param idRecepta
	 * @return
	 */
	public Respuesta deleteLikes(Integer idUsuari, Integer idRecepta) {

		Statement statement = null;
		Integer result = 0;
		boolean resultBoolean = false;

		try {

			statement = connection.createStatement();
			result = statement.executeUpdate(DELETE_LIKE + idUsuari + AND_ID_RECEPTA + idRecepta);
			resultBoolean = (result != 0) ? true : false;

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}
		
		Respuesta respuesta = new Respuesta(resultBoolean);
		
		return respuesta;
	}

	// ----------------------- FIN METHODs LIKES ------------------------//

	// ----------------------- INICIO METHODs SEGUIDORS -------------------------//

	/**
	 * 
	 * Comprueba si un usuario es seguido por otro usuario 
	 * 
	 * @param idUsuari
	 * @param idUsuariSeguit
	 * @return boolean
	 */
	private boolean isFollow(Integer idUsuari, Integer idUsuariSeguit) {

		boolean isFollow = false;
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT id_usuari, id_usuari_seguit FROM seguidors WHERE id_usuari = "
					+ idUsuari + " AND id_usuari_seguit = " + idUsuariSeguit);
			isFollow = (resultSet.next()) ? true : false;

			LOGGER.info(" [ MQM ] ISFOLLOW : " + isFollow);
			LOGGER.info(" [ MQM ] user : " + idUsuari);
			LOGGER.info(" [ MQM ] userSeguit : " + idUsuariSeguit);

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		}

		return isFollow;
	}

	/**
	 * 
	 * Insertar un seguidor PARAMS [0] id_usuari PARAMS [1] id_usuari_seguit
	 * 
	 * @param params
	 * @return Respuesta
	 */
	public synchronized Respuesta insertSeguidors(Integer... params) {

		Integer result = -1;
		Statement statement = null;

		try {
			statement = connection.createStatement();
			result = statement.executeUpdate(INSERT_SEGUIDORS + VAL_OP + params[0] + SEP + params[1] + PAR_CL);

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}
		
		Respuesta respuestaInsert = new Respuesta((result!=-1) ? true : false );
		return respuestaInsert;
	}

	/**
	 * 
	 * Selecciona la cantidad total de usuarios que siguen al idUsuari
	 * 
	 * @param id_usuari
	 * @return Integer
	 */
	public Integer selectCntTotalSeguidors(Integer id_usuari) {

		Integer result = -1;
		Statement statement = null;
		ResultSet resultSet = null;
				
		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_CNT_TOTAL_SEGUIDORS + id_usuari);
			result = (resultSet.next()) ? resultSet.getInt(1) : result;
			
		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(SQL);
					sql.printStackTrace();
				}
			}
		}
		
		return result;

	}

	/**
	 * 
	 * Eliminar el seguimiento de un usuario a otro
	 * 
	 * @param idUsuari
	 * @param idUsuariSeguit
	 * @return
	 */
	public Respuesta deleteSeguidors(Integer idUsuari, Integer idUsuariSeguit) {

		Statement statement = null;
		Integer result = 0;
		boolean resultBoolean = false;

		try {

			statement = connection.createStatement();
			result = statement.executeUpdate(DELETE_SEGUIDOR + idUsuari + AND_ID_USUARISEGUIT + idUsuariSeguit);
			resultBoolean = (result != 0) ? true : false;

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		Respuesta respuesta = new Respuesta(resultBoolean);
		return respuesta;
	}

	/**
	 * 
	 * Seleccionar una lista de usuarios seguidos
	 * 
	 * @param idUsuari
	 * @return ArrayList<Usuari> 
	 */
	public synchronized ArrayList<Usuari> selectUsuarisSeguits(Integer idUsuari) {

		ArrayList<Usuari> listUsuaris = new ArrayList<>();
		Statement statement = null;
		ResultSet resultSet = null;

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_USUARIOS_SEGUITS + idUsuari + ORDER_BY_USER + PAR_CL);
			Integer idUsuariSeguit = null;
			String nomUsuari = null;
			Integer cantidadReceptes = 0;
			boolean isFollow = true;

			while (resultSet.next()) {

				idUsuariSeguit = resultSet.getInt(1);
				nomUsuari = resultSet.getString(2);
				cantidadReceptes = selectCntReceptesByUser(idUsuariSeguit);

				Usuari usuari = new Usuari(idUsuariSeguit, cantidadReceptes, nomUsuari, isFollow);
				listUsuaris.add(usuari);

			}

		} catch (PSQLException psql) {
			LOGGER.info(PSQL);
			psql.printStackTrace();
		} catch (SQLException sql) {
			LOGGER.info(PSQL);
			sql.printStackTrace();
		} finally {

			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return listUsuaris;
	}

	/**
	 * 
	 * Seleccionar una lista de todos los usuarios ( Seguidos y No seguidos )
	 * 
	 * @param idUsuari
	 * @return ArrayList<Usuari>
	 */
	public synchronized ArrayList<Usuari> selectAllUsuaris(Integer idUsuari) {

		ArrayList<Usuari> listUsuaris = new ArrayList<>();
		Statement statement = null;
		ResultSet resultSet = null;
		Integer cantidadReceptes = 0;

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL_USUARIOS_SEGUITS + idUsuari
					+ COMPLEMENT_SELECT_ALL_USUARIOS_ADD + idUsuari + ORDER_BY_USER);
			Integer idUsuariSeguit = null;
			String nomUsuari = null;
			boolean isFollow = false;

			while (resultSet.next()) {

				idUsuariSeguit = resultSet.getInt(1);
				nomUsuari = resultSet.getString(2);
				isFollow = resultSet.getBoolean(3);
				cantidadReceptes = selectCntReceptesByUser(idUsuariSeguit);

				Usuari usuari = new Usuari(idUsuariSeguit, cantidadReceptes, nomUsuari, isFollow);
				listUsuaris.add(usuari);

			}

		} catch (PSQLException psql) {
			LOGGER.info(PSQL);
			psql.printStackTrace();
		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {

			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(SQL);
					sql.printStackTrace();
				}
			}

		}

		return listUsuaris;

	}

	//

	// ----------------------- FIN METHODs SEGUIDORS ------------------------//

	// ----------------------- INICIO METHODs IMAGEs ------------------------//

	/**
	 * 
	 * Seleccionar una imagen por idReceta
	 * 
	 * @param id_receta
	 * @return byte []
	 */
	public byte[] selectImageByIdRecepta(Integer id_recepta) {

		Statement statement = null;
		ResultSet resultSet = null;
		byte[] image = null;

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_RECEPTES_IMAGE + id_recepta);

			if (resultSet.next()) {
				image = resultSet.getBytes(IMATGE_RECEPTA);
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(SQL);
					sql.printStackTrace();
				}
			}
		}

		return image;

	}	

	/**
	 * 
	 * Seleccionar una imagen de pasos
	 * 
	 * @param id_recepta
	 * @param id_pas
	 * @return byte []
	 */
	public byte[] selectImagesByIdPas(Integer id_recepta, Integer id_pas) {

		byte[] image = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_PASSOS_IMAGE + id_pas + AND_ID_RECEPTA + id_recepta);

			if (resultSet.next()) {
				image = resultSet.getBytes(IMATGE_PAS);
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return image;
	}

	// ----------------------- FIN METHODs IMAGE's ------------------------//

	// ----------------------- RECEPTA SORT FILTROS ------------------------//

	
	/**
	 * 
	 * Selecciona una lista de recetas por filtrado
	 * 
	 * @param filtres
	 * @return
	 */
	public ArrayList<Recepta> selectSortReceptes(String filtres) {

		ArrayList<Recepta> listSortReceptes = new ArrayList<>();
		ResultSet resultSet = null;
		Recepta recepta = null;
		Integer id_recepta = null;
		String titol_recepta = null;
		Statement statement = null;

		try {

			statement = connection.createStatement();

			switch (filtres) {
			case SELECT_SORT_RECEPTES_NOVEDADES:
				resultSet = statement.executeQuery(SORT_NOVEDADES);
				break;
				
			case SELECT_SORT_RECEPTES_TOP_LIKES:
				resultSet = statement.executeQuery(SORT_TOPLIKE);
				break;

			case SELECT_SORT_RECEPTES_ENTRANTE:
				resultSet = statement.executeQuery(SORT_ENTRANTE);
				break;

			case SELECT_SORT_RECEPTES_SEGUNDO:
				resultSet = statement.executeQuery(SORT_SEGUNDO);
				break;

			case SELECT_SORT_RECEPTES_POSTRE:
				resultSet = statement.executeQuery(SORT_POSTRE);
				break;

			}

			if (resultSet!=null) {
				while (resultSet.next()) {
					id_recepta = resultSet.getInt(1);
					titol_recepta = resultSet.getString(2);
					recepta = new Recepta(id_recepta, titol_recepta);
					listSortReceptes.add(recepta);
				}
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();					
				}
			}
		}
		return listSortReceptes;
	}

	
	/**
	 * Selecciona una lista recetas del usuario
	 * 
	 * @param filtres
	 * @param idUsuari
	 * @return ArrayList<Recepta>
	 */
	public ArrayList<Recepta> selectSortByUser(String filtres, Integer idUsuari) {

		ArrayList<Recepta> listSortReceptes = new ArrayList<>();
		ResultSet resultSet = null;
		Recepta recepta = null;
		Integer id_recepta = null;
		String titol_recepta = null;
		
		Statement statement = null;

		try {

			statement = connection.createStatement();

			switch (filtres) {

			case SELECT_SORT_RECEPTES_MIS_RECEPTES:
				resultSet = statement.executeQuery(SELECT_RECEPTES_BYUSER + idUsuari + ORDER_BY_RECEPTA + DESC);
				break;

			case SELECT_SORT_RECEPTES_MIS_LIKES:
				resultSet = statement.executeQuery(SORT_MISLIKES + idUsuari + PAR_CL);
				break;

			}
			if (resultSet!=null) {
				while (resultSet.next()) {
					id_recepta = resultSet.getInt(1);
					titol_recepta = resultSet.getString(2);
					recepta = new Recepta(id_recepta, titol_recepta);
					listSortReceptes.add(recepta);
				}
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}
		return listSortReceptes;
	}

	
	
	// --------------- METHODs USUARIO

	/**
	 * 
	 * Inserción de un usuario
	 * 
	 * @param usuari
	 * @param token
	 * @return Respuesta
	 */
	public Respuesta InsertUsuari(Usuari usuari, String token) {

		Respuesta respuesta = null;
		Integer result = null;

		String nomUsuari = usuari.getNom_usuari();
		String contrasenyaUsuari = usuari.getContrasenya_usuari();
		String mailUsuari = usuari.getMail_usuari();

		Statement statementCreate = null;

		try {

			statementCreate = connection.createStatement();
			result = statementCreate.executeUpdate(
					INSERT_USUARIOS + VAL_OP + SQUOTE + nomUsuari + SQUOTE + SEP + SQUOTE + contrasenyaUsuari + SQUOTE
							+ SEP + SQUOTE + mailUsuari + SQUOTE + SEP + SQUOTE + token + SQUOTE + PAR_CL);
			respuesta = new Respuesta((result != 0) ? true : false);

		} catch (PSQLException psql) {

			respuesta = new Respuesta(false);

			return respuesta;

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statementCreate != null) {
				try {
					statementCreate.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return respuesta;
	}

	/**
	 * 
	 * Realiza la comprobación de acceso al usuario	
	 * 
	 * @param usuari
	 * @return Usuari
	 */
	public synchronized Usuari loginUsuari(Usuari usuari) {

		Respuesta respuesta = null;
		ResultSet resultSet = null;

		String nomUsuari = usuari.getNom_usuari();
		String contrasenyaUsuari = usuari.getContrasenya_usuari();

		Statement statement = null;

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_USUARIO_CHECK + SQUOTE + nomUsuari + SQUOTE + AND_PASS + SQUOTE
					+ contrasenyaUsuari + SQUOTE);

			if (resultSet.next()) {

				usuari.setId_usuari(resultSet.getInt(1));
				usuari.setMail_usuari(resultSet.getString(4));
				usuari.setToken(resultSet.getString(5));
				respuesta = new Respuesta(true);
				usuari.setRespuesta(respuesta);

			} else {
				respuesta = new Respuesta(false);
				usuari.setRespuesta(respuesta);
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return usuari;
	}

	/**
	 * 
	 * Seleccionar un usuario
	 * 
	 * @param usuari
	 * @return Usuari
	 */
	public synchronized Usuari selectUsuari(Usuari usuari) {

		Respuesta respuesta = null;
		ResultSet resultSet = null;

		Integer idUsuari = usuari.getId_usuari();
		Statement statement = null;

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_CNT_RECEPTES_AND_SEGUIDORS_AND_LIKES + idUsuari);

			if (resultSet.next()) {

				usuari.setCantidadReceptas(resultSet.getInt(1));
				usuari.setCantidadSeguidores(resultSet.getInt(2));
				usuari.setCantidadLikes(resultSet.getInt(3));

				respuesta = new Respuesta(true);
				usuari.setRespuesta(respuesta);

			} else {

				respuesta = new Respuesta(false);
				usuari.setRespuesta(respuesta);

				usuari.setCantidadReceptas(0);
				usuari.setCantidadSeguidores(0);

			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return usuari;
	}

	/**
	 * 
	 * Compración de token del usuario
	 * 
	 * @param usuari
	 * @return Usuari
	 */
	public synchronized Usuari loginTokenUsuari(Usuari usuari) {

		Respuesta respuesta = null;
		ResultSet resultSet = null;

		String token = usuari.getToken();

		Statement statement = null;

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_USUARIO_TOKEN + SQUOTE + token + SQUOTE);

			if (resultSet.next()) {

				respuesta = new Respuesta(true);
				usuari.setRespuesta(respuesta);

			} else {

				respuesta = new Respuesta(false);
				usuari.setRespuesta(respuesta);

			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return usuari;
	}

	
	/**
	 * 
	 * Comprobación contraseña del usuario
	 * 
	 * @param usuari
	 * @return Respuesta
	 */
	public synchronized Respuesta comprobarContrasenyaUsuari(Usuari usuari) {

		Statement statement = null;
		Respuesta respuesta = new Respuesta(false);
		ResultSet resultSet = null;
		String contrasenyaUsuari = usuari.getContrasenya_usuari();
		String token = usuari.getToken();

		try {

			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT + ID_USUARI + FROM + TABLE_USUARIS + WHERE + CONTRASENYA_USUARI
					+ EQUAL + SQUOTE + contrasenyaUsuari + SQUOTE + AND + TOKEN + EQUAL + SQUOTE + token + SQUOTE);
			if (resultSet.next()) {
				respuesta = new Respuesta(true);
			} else {
				respuesta.setRespuestaControl(0);
			}

		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return respuesta;
	}

	/**
	 * 
	 * Actualizar un usuario
	 * 
	 * @param usuari
	 * @return Respuesta
	 */
	public synchronized Respuesta updateUsuari(Usuari usuari) {

		Respuesta respuesta = new Respuesta(false);
		Statement statement = null;
		Integer result = 0;

		String tokenUsuari = usuari.getToken();
		String nomUsuari = usuari.getNom_usuari();
		String contrasenyaUsuari = usuari.getContrasenya_usuari();
		String mailUsuari = usuari.getMail_usuari();

		String queryToUpdate = null;

		if (!(contrasenyaUsuari.equals(""))) {

			LOGGER.info(PROFILE_UPDT_PASS);

			queryToUpdate = UPDATE_USUARIOS + NOM_USUARI + EQUAL + SQUOTE + nomUsuari + SQUOTE + SEP
					+ CONTRASENYA_USUARI + EQUAL + SQUOTE + contrasenyaUsuari  + SQUOTE + SEP + MAIL_USUARI + EQUAL + SQUOTE + mailUsuari
					+ SQUOTE + WHERE + TOKEN + EQUAL + SQUOTE + tokenUsuari + SQUOTE;

		} else {

			LOGGER.info(PROFILE_UPDT);

			queryToUpdate = UPDATE_USUARIOS + NOM_USUARI + EQUAL + SQUOTE + nomUsuari + SQUOTE + SEP + MAIL_USUARI
					+ EQUAL + SQUOTE + mailUsuari + SQUOTE + WHERE + TOKEN + EQUAL + SQUOTE + tokenUsuari + SQUOTE;
		}

		try {

			statement = connection.createStatement();
			result = statement.executeUpdate(queryToUpdate);

			if (result != 0) {
				respuesta = new Respuesta(true);
			}

		} catch (PSQLException psql) {
			respuesta.setRespuestaControl(1);
			LOGGER.info(PSQL);
			psql.printStackTrace();
		} catch (SQLException sql) {
			LOGGER.info(SQL);
			sql.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sql) {
					LOGGER.info(S);
					sql.printStackTrace();
				}
			}
		}

		return respuesta;

	}
	
	/**
	 * Cierre de connexión con AutoClosable
	 */
	@Override
	public void close() throws Exception {
		connection.close();
	}

}
