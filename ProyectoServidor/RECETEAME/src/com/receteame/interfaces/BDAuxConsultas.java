package com.receteame.interfaces;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public interface BDAuxConsultas extends DBGenericTables {

	public static final String PAR_OP = " ( ";
	public static final String SEP = " , ";
	public static final String SEP_Q = " , \'";
	public static final String SEP_D_Q = "' , \'";
	
	public static final String SQUOTE = "\'";
	public static final String DQUOTE = "\"";
	
	public static final String VAL = " ? ";
	public static final String PAR_CL = " ) ";
	public static final String GREATER = " > " ;
	public static final String INSERT = " INSERT  INTO ";
	public static final String SELECT = " SELECT ";
	public static final String UPDATE = " UPDATE ";
	public static final String DELETE = " DELETE FROM ";
	public static final String VAL_OP = " VALUES ( ";	
	public static final String SET = " SET ";
	public static final String FROM = " FROM ";
	public static final String WHERE = " WHERE ";
	public static final String AND = " AND ";
	public static final String LIKE = " LIKE ";
	public static final String EQUAL =  " = ";
	public static final String CNT_OP = " COUNT ( ";
	public static final String NOT_EQUAL = "!=";
	public static final String ALL = "ALL";
	public static final String ANY = "ANY";
	
	public static final String AS = " AS ";
	public static final String NUM_RECEPTES = " num_receptes ";
	public static final String NUM_SEGUIDORS = " num_seguidors ";
	public static final String NUM_LIKES = "num_likes";
	
	public static final String IN = " IN ";
	public static final String DESC = " DESC ";
	public static final String RETURNING = " RETURNING ";
	public static final String ORDER_BY = " ORDER BY ";
	public static final String GROUP_BY = " GROUP BY ";
	public static final String HAVING = " HAVING ";
	public static final String COUNT_ALL = " COUNT(*) "; 
	public static final String DOT = ".";
	
	public static final String RELATION = " Z ";
	public static final String RRECETA = " R ";
	public static final String RLIKE = " L ";
	public static final String RUSER = " U ";
	
	
	//AGRUPACIÓN DE CAMPOS PARA INSERTAR
	public static final String FIELDS_INSERT_RECEPTES = ID_USUARI+SEP+TITOL_RECEPTA+SEP+NUMERO_COMENSALS+SEP+CATEGORIA_MENU+SEP+IMATGE_RECEPTA;
	public static final String FIELDS_INSERT_PASSOS = ID_RECEPTA+SEP+ID_PAS+SEP+DESCRIPCIO_PAS+SEP+IMATGE_PAS;
	public static final String FIELDS_INSERT_USUARIS = NOM_USUARI+SEP+CONTRASENYA_USUARI+SEP+MAIL_USUARI+SEP+TOKEN;
	public static final String FIELDS_INSERT_SEGUIDORS = ID_USUARI+SEP+ID_USUARI_SEGUIT;
	public static final String FIELDS_INSERT_INGREDIENTS = ID_RECEPTA+SEP+NOM_INGREDIENT+SEP+QUANTITAT+SEP+TIPUS_QUANTITAT;
	public static final String FIELDS_INSERT_LIKES = ID_USUARI+SEP+ID_RECEPTA;
		
	//AGRUPACIÓN DE CAMPOS PARA SELECCIONAR
	public static final String ALL_FIELDS_RECEPTES = ID_RECEPTA+SEP+ID_USUARI+SEP+TITOL_RECEPTA+SEP+NUMERO_COMENSALS+SEP+DATA_CREACIO_RECEPTA+SEP+CATEGORIA_MENU+SEP+IMATGE_RECEPTA;
	public static final String ALL_FIELDS_INGREDIENTS = ID_INGREDIENT+SEP+ID_RECEPTA+SEP+NOM_INGREDIENT+SEP+QUANTITAT+SEP+TIPUS_QUANTITAT;
	public static final String ALL_FIELDS_PASSOS = ID_RECEPTA+SEP+ID_PAS+SEP+DESCRIPCIO_PAS+SEP+IMATGE_PAS;
	public static final String ALL_FIELDS_USUARIS = ID_USUARI+SEP+NOM_USUARI+SEP+CONTRASENYA_USUARI+SEP;
	public static final String ALL_FIELDS_SEGUIDORS = ID_USUARI+SEP+ID_USUARI_SEGUIT;
	public static final String ALL_FIELDS_LIKES = ID_USUARI+SEP+ID_RECEPTA;

	//AND's
	public static final String AND_ID_RECEPTA = AND+ID_RECEPTA+EQUAL;
	public static final String AND_ID_USUARISEGUIT = AND+ID_USUARI_SEGUIT+EQUAL;
	
	public static final String ORDER_BY_RECEPTA = ORDER_BY+ID_RECEPTA;
	public static final String ORDER_BY_USER = ORDER_BY+NOM_USUARI;
}
