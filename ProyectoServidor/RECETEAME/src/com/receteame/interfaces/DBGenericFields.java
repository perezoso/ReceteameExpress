package com.receteame.interfaces;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public interface DBGenericFields {
	
	public static final String DQUOTE = "\"";
	public static final String SQUOTE = "\'";
	
	//GENERAL
	public static final String ID_USUARI = DQUOTE+"id_usuari"+DQUOTE;
    //USUARI TABLE
	public static final String NOM_USUARI = DQUOTE+"nom_usuari"+DQUOTE;
	public static final String CONTRASENYA_USUARI = "contrasenya_usuari";
	public static final String MAIL_USUARI = "mail_usuari";
	public static final String TOKEN = "token_usuari";
	//SEGUIDORS TABLE
	public static final String ID_USUARI_SEGUIT = "id_usuari_seguit";
	//RECEPTES TABLE	
	public static final String ID_RECEPTA = "id_recepta";
	public static final String TITOL_RECEPTA = DQUOTE+"titol_recepta"+DQUOTE;
	public static final String NUMERO_COMENSALS = "numero_comensals";
	public static final String DATA_CREACIO_RECEPTA = "data_creacio_recepta";
	public static final String CATEGORIA_MENU = "categoria_menu";
	public static final String IMATGE_RECEPTA = "imatge_recepta";
	//VALUES
	public static final String PRIMER_PLATO = SQUOTE+"Primer plato"+SQUOTE;
	public static final String SEGUNDO_PLATO = SQUOTE+"Segundo plato"+SQUOTE;
	public static final String POSTRE = SQUOTE+"Postre"+SQUOTE;
	//PASSOS TABLE
	public static final String ID_PAS = "id_pas";
	public static final String DESCRIPCIO_PAS = "descripcio_pas";
	public static final String IMATGE_PAS = "imatge_pas";
	//INGREDIENTS TABLE
	public static final String ID_INGREDIENT = "id_ingredient";
	public static final String NOM_INGREDIENT = "nom_ingredient";
	public static final String QUANTITAT = "quantitat";
	public static final String TIPUS_QUANTITAT = "tipus_quantitat";
	
}
