package com.receteame.interfaces;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public interface LoggerMessages {
	
	public final static String INR = " [ MQM ] @InsertRecepta  [ SQL ] ";
	public final static String DATOS_RECETA = " [ MQM ] || @Recepta#1 || OK ";
	public final static String DATOS_PASOS = " [ MQM ] || @Recepta#2 || OK ";
	public final static String DATOS_INGREDIENTES = " [ MQM ] || @Recepta#3 || OK ";
	public final static String INSERTANDO_INGREDIENTES = " [ MQM ] || Insertando lista de ingredientes de la receta ";
	public final static String INSERTANDO_PASOS = " [ MQM ] || Insertando lista de pasos de la receta ";
	
	public final static String DATOS_RECETA_UPDT = " [ MQM ] || @Update #1 || ";
	public final static String DATOS_PASOS_UPDT = " [ MQM ] || @Update #2 || ";
	public final static String DATOS_INGREDIENTES_UPDT = " [ MQM ] || @Update #3 || ";
	public final static String DATOS_UPDT = " [ MQM ] || @Update Respuesta || ";
	
	public final static String PROFILE_UPDT_PASS = " [ MQM ] UPDATE PERFIL + CAMBIO CONTRASENYA ";
	public final static String PROFILE_UPDT=  " [ MQM ] UPDATE PERFIL";			

	public final static String SRC_FILTRES = "[ Started ] || SRCFilter";
	public final static String SRC_IMAGEN = "[ Started ] || SRCImagen";
	public final static String SRC_LIKES = "[ Started ] || SRCLikes";
	public final static String SRC_SEGUIDOR = "[ Started ] || SRCSeguidor";
	public final static String SRC_USERPROFILE = "[ Started ] || SRCUserProfile";
	public final static String SRC_RECEPTA = "[ Started ] || SRCRecepta";
	
	public final static String SQL = " [ MQM ] SQL problema en la ejecución consulta ";
	public final static String PSQL = " [ MQM ] PSQL problema en la consulta ";
	public final static String PS = " [ MQM ] Prepared Statement problema .close() ";
	public final static String S = " [ MQM ] Statement problema .close() ";

}
