package com.receteame.interfaces;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public interface  DBGenericTables extends DBGenericFields {
	
	//TABLAS EN NUESTRA BASE DE DATOS
	public static final String TABLE_RECEPTES = "receptes";
	public static final String TABLE_INGREDIENTS = "ingredients";
	public static final String TABLE_PASSOS = "passos";
	public static final String TABLE_LIKES = "likes";
	public static final String TABLE_USUARIS = "usuaris";
	public static final String TABLE_SEGUIDORS = "seguidors";
	
}
