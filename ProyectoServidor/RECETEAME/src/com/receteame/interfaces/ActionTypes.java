package com.receteame.interfaces;

import java.util.logging.Logger;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
public interface ActionTypes {
	
	public static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	//ENCTYPE-CODE
	public static final String CONTENT_TYPE = "text/html;charset=UTF-8";
	//ACTION
	public static final String ACTION = "action";
	//GENERAL
	public static final String INSERT = "insert";
	public static final String DELETE = "delete";
	//USUARI
	public static final String CREAR_USUARIO = "crearUsuario";
	public static final String LOGIN_USUARIO = "loginUsuario";
	public static final String SELECT_USUARIO = "selectUsuario";
	public static final String UPDATE_USUARIO = "updateUsuario";
	public static final String LOGIN_TOKEN_USUARIO = "loginTokenUsuario";
	//SELECT - SRCRecepta
	public static final String SELECT_RECEPTA = "selectRecepta";
	public static final String SELECT_RECEPTA_AJENA = "selectReceptaAjena";
	public static final String DELETE_RECEPTA = "deleteRecepta";
	public static final String UPDATE_RECEPTA = "updateRecepta";
	//SELECT - SRCLikes
	public static final String SELECT_LIKE_RECEPTA = "selectLikeRecepta";
	public static final String SELECT_TOTAL_LIKES = "selectTotalLikesRecepta";
	//SELECT - SRCSeguidors
	public static final String SELECT_SEGUIDORS = "selectTotalSeguidors";
	public static final String SELECT_USUARIOS_SEGUIDOS = "selectUsuariosSeguidos";
	public static final String SELECT_ALL_USUARIOS = "selectAllUsuarios";
	//IMG - SRCImagen
	public static final String SELECT_RECEPTA_IMG = "selectReceptaImg";
	public static final String SELECT_PASSOS_IMG = "selectPassosImg";
	//SORT - SRCFILTER
	public static final String SELECT_SORT_RECEPTES_MIS_RECEPTES = "Mis Recetas";
	public static final String SELECT_SORT_RECEPTES_NOVEDADES = "Novedades";
	public static final String SELECT_SORT_RECEPTES_MIS_LIKES = "Mis likes";
	public static final String SELECT_SORT_RECEPTES_TOP_LIKES = "Top likes";
	public static final String SELECT_SORT_RECEPTES_ENTRANTE ="Primer plato";
	public static final String SELECT_SORT_RECEPTES_SEGUNDO = "Segundo plato";
	public static final String SELECT_SORT_RECEPTES_POSTRE = "Postre";
}
