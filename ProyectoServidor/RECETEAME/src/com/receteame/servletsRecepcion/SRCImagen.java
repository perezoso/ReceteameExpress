package com.receteame.servletsRecepcion;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.receteame.controlSRDB.SRDBControl;
import com.receteame.interfaces.ActionTypes;

import static com.receteame.interfaces.LoggerMessages.SRC_IMAGEN;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
@WebServlet("/SRCImagen")
@MultipartConfig(
		fileSizeThreshold = 1024 * 1024 * 10, // 10MB
		maxFileSize = 20_971_520L, // 20MB
		maxRequestSize = 41_943_040L, // 40MB
		location = "/tmp"
	)
public class SRCImagen extends HttpServlet implements ActionTypes {
	
	private static final long serialVersionUID = 1L;
    
	/**
	 * Inicialización servidor
	 */
	public void init() {
		LOGGER.info(SRC_IMAGEN);
		new SRDBControl();
	}
		
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String action = null;		
		Integer idRecepta = null;
		Integer idPas = null;

		action = request.getParameter("action");

		if (action!=null)
			switch (action) {
			
			case SELECT_RECEPTA_IMG:
				idRecepta = Integer.parseInt(request.getParameter("idRep"));
				printImageRecepta(idRecepta, response);
				break;
	
			case SELECT_PASSOS_IMG:
				idRecepta = Integer.parseInt(request.getParameter("idRep"));
				idPas = Integer.parseInt(request.getParameter("idPas"));
				printImageReceptaPas(idRecepta, idPas, response);
				break;
				
			default : 
			
			}

	}

	/**
	 * 
	 * Sirve una imagen paso , para picasso ( Android )
	 * 
	 * @param idRecepta
	 * @param idPas
	 * @param response
	 */
	private void printImageReceptaPas(Integer idRecepta, Integer idPas, HttpServletResponse response) {

		byte [] bytes = SRDBControl.masterQueryManagement.selectImagesByIdPas(idRecepta,idPas);
		
		OutputStream output;
		try {
			output = response.getOutputStream();
			output.write(bytes, 0, bytes.length);
			output.flush();
			output.close();
		} catch (NullPointerException n) {
			n.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Sirve una imagen receta , para picasso ( Android )
	 * 
	 * @param idRecepta
	 * @param response
	 */
	protected synchronized void printImageRecepta (Integer idRecepta, HttpServletResponse response) {
		
		byte[] bytes = SRDBControl.masterQueryManagement.selectImageByIdRecepta(idRecepta);

		OutputStream output = null;
			
			try {
				output = response.getOutputStream();
				output.write(bytes, 0, bytes.length);
				output.flush();
				output.close();
			} catch (NullPointerException n) {
				n.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
			
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
