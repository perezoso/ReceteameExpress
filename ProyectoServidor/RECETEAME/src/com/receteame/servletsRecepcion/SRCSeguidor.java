package com.receteame.servletsRecepcion;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.receteame.JavaPojos.Action;
import com.receteame.JavaPojos.Seguidor;
import com.receteame.controlSRDB.SRDBControl;
import com.receteame.interfaces.ActionTypes;

import static com.receteame.interfaces.LoggerMessages.SRC_SEGUIDOR;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
@WebServlet("/SRCSeguidor")
@MultipartConfig
public class SRCSeguidor extends HttpServlet implements ActionTypes {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Inicialización servidor
	 */
	public void init() {
		LOGGER.info(SRC_SEGUIDOR);
		new SRDBControl();
	}
	
	private void print (String json,HttpServletResponse response) {
		try {
			response.getWriter().print(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// NOTHING
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType(CONTENT_TYPE);
        
		Part partAction = request.getPart("action");
	    Part partSeguidor = request.getPart("seguidor");

	    Action action = SRDBControl.parseObjectAction(partAction.getInputStream(),Action.class);
	    Seguidor seguidor = SRDBControl.parseObjectSeguidor(partSeguidor.getInputStream(),Seguidor.class);
	    
	    switch (action.getAction()) {
	    
			case INSERT:
					Integer idUsuariInsert = seguidor.getId_usuari();
					Integer idUsuariSeguitInsert = seguidor.getId_usuari_seguit();
					String jsonInsert = SRDBControl.getAnswerRespuesta(SRDBControl.masterQueryManagement.insertSeguidors(idUsuariInsert,idUsuariSeguitInsert));				
					print(jsonInsert, response);
				break;

			case SELECT_SEGUIDORS:
					Integer idUsuariSelectTotalSeguidors = seguidor.getId_usuari();
					String jsonSelectSeguidors = SRDBControl.getAnswerRespuestaInteger(SRDBControl.masterQueryManagement.selectCntTotalSeguidors(idUsuariSelectTotalSeguidors));
					print(jsonSelectSeguidors, response);
				break;
				
			case SELECT_USUARIOS_SEGUIDOS:
					Integer idUsuari = seguidor.getId_usuari();
					String jsonUsuarisSeguits = SRDBControl.getAnswerListUsuari(SRDBControl.masterQueryManagement.selectUsuarisSeguits(idUsuari));
					print(jsonUsuarisSeguits, response);

				break;

			case SELECT_ALL_USUARIOS:
					Integer idUsuariAll = seguidor.getId_usuari();
					String jsonSelectUsuaris = SRDBControl.getAnswerListUsuari(SRDBControl.masterQueryManagement.selectAllUsuaris(idUsuariAll));
					print(jsonSelectUsuaris, response);
				break;
				
			case DELETE:
					Integer idUsuariDelete = seguidor.getId_usuari();
					Integer idUsuariSeguitDelete = seguidor.getId_usuari_seguit();
					String jsonDelete = SRDBControl.getAnswerRespuesta(SRDBControl.masterQueryManagement.deleteSeguidors(idUsuariDelete,idUsuariSeguitDelete));				
					print(jsonDelete, response);
				break;
				
			default: 
				break;		
	    }
	}	
}
