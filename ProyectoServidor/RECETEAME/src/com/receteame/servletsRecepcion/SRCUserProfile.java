package com.receteame.servletsRecepcion;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.receteame.JavaPojos.Action;
import com.receteame.JavaPojos.Usuari;
import com.receteame.controlSRDB.SRDBControl;
import com.receteame.interfaces.ActionTypes;

import static com.receteame.interfaces.LoggerMessages.SRC_USERPROFILE;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
@WebServlet("/SRCUserProfile")
@MultipartConfig
public class SRCUserProfile extends HttpServlet implements ActionTypes {
	
	private static final long serialVersionUID = 1L;
	private SecureRandom random = null;
	
	/**
	 * Inicialización servidor
	 */
	public void init() {
		LOGGER.info(SRC_USERPROFILE);
		new SRDBControl();
		random = new SecureRandom();
	}
	
	private void print (String json,HttpServletResponse response) {
		try {
			response.getWriter().print(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//NOTHING
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType(CONTENT_TYPE);

		Part partAction = request.getPart("action");
		Part partUsuari = request.getPart("usuario");

		Action action = SRDBControl.parseObjectAction(partAction.getInputStream(), Action.class);
		Usuari usuari = SRDBControl.parseObjectUsuari(partUsuari.getInputStream(), Usuari.class);

		switch (action.getAction()) {

		case CREAR_USUARIO:
			String jsonCrear = SRDBControl
					.getAnswerRespuesta(SRDBControl.masterQueryManagement.InsertUsuari(usuari, nextSessionId()));
			print(jsonCrear, response);
			break;

		case LOGIN_USUARIO:
			String jsonLogUsuari = SRDBControl.getAnswerUsuario(SRDBControl.masterQueryManagement.loginUsuari(usuari));
			print(jsonLogUsuari, response);
			break;

		case SELECT_USUARIO:
			String jsonSelectUsuari = SRDBControl
					.getAnswerUsuario(SRDBControl.masterQueryManagement.selectUsuari(usuari));
			print(jsonSelectUsuari, response);
			break;

		case LOGIN_TOKEN_USUARIO:
			String jsontokenUsuari = SRDBControl
					.getAnswerUsuario(SRDBControl.masterQueryManagement.loginTokenUsuari(usuari));
			print(jsontokenUsuari, response);
			break;

		case UPDATE_USUARIO:
		
			if (SRDBControl.masterQueryManagement.comprobarContrasenyaUsuari(usuari).isRespuesta()) {
				Part partUsuariModificat = request.getPart("usuarioModificado");
				Usuari usuariModificat = SRDBControl.parseObjectUsuari(partUsuariModificat.getInputStream(),Usuari.class);
				String jsonUpdate = SRDBControl.getAnswerRespuesta(SRDBControl.masterQueryManagement.updateUsuari(usuariModificat));
				print(jsonUpdate, response);
			} else {
				print(SRDBControl.getAnswerRespuestaFalse(), response);
			}

			break;

		}

		doGet(request, response);
	}
	
	/**
	 * Obtiene un alfanumerico aleatorio 
	 * 
	 * @return
	 */
	public synchronized String nextSessionId() {
	    return new BigInteger(130, random).toString(32);
	}
	
}
