package com.receteame.servletsRecepcion;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.receteame.JavaPojos.Action;
import com.receteame.JavaPojos.Like;
import com.receteame.controlSRDB.SRDBControl;
import com.receteame.interfaces.ActionTypes;
import static com.receteame.interfaces.LoggerMessages.SRC_LIKES;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
@WebServlet("/SRCLikes")
@MultipartConfig
public class SRCLikes extends HttpServlet implements ActionTypes {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Inicialización servidor
	 */
	public void init() {
		new SRDBControl();
		LOGGER.info(SRC_LIKES);
	}
	
	private void print (String json,HttpServletResponse response) {
		try {
			response.getWriter().print(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//NOTHING
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType(CONTENT_TYPE);
        
		Part partAction = request.getPart("action");
	    Part partLike = request.getPart("like");     

	    Action action = SRDBControl.parseObjectAction(partAction.getInputStream(),Action.class);
	    Like like = SRDBControl.parseObjectLike(partLike.getInputStream(),Like.class);

		switch (action.getAction()) {
			case INSERT:
					Integer idUsuarioInsert = like.getId_usuari();
					Integer idReceptaInsert = like.getId_recepta();		
					String jsonInsert = SRDBControl.getAnswerRespuesta(SRDBControl.masterQueryManagement.insertLikes(idUsuarioInsert,idReceptaInsert));				
					print(jsonInsert, response);
				break;
	
			case SELECT_LIKE_RECEPTA:
					Integer idReceptaSelectLikes = like.getId_recepta();
					String jsonSelectLike = SRDBControl.getAnswerRespuestaInteger(SRDBControl.masterQueryManagement.selectCntLikesRecepta(idReceptaSelectLikes));
					print(jsonSelectLike, response);		
				break;
				
			case SELECT_TOTAL_LIKES: 
					Integer idUsuarioSelectTotal = like.getId_usuari();
					String jsonSelectLikes = SRDBControl.getAnswerRespuestaInteger(SRDBControl.masterQueryManagement.selectCntTotalLikes(idUsuarioSelectTotal));
					print(jsonSelectLikes, response);
				break;
				
			case DELETE: 
					Integer idUsuarioDelete = like.getId_usuari();
					Integer idReceptaDelete = like.getId_recepta();		
					String jsonDelete = SRDBControl.getAnswerRespuesta(SRDBControl.masterQueryManagement.deleteLikes(idUsuarioDelete,idReceptaDelete));				
					print(jsonDelete, response);
				break;
				
			default: 
				break;		
		}
		
		doGet(request, response);
	}
	
}
