package com.receteame.servletsRecepcion;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.receteame.JavaPojos.Filtres;
import com.receteame.JavaPojos.Usuari;
import com.receteame.controlSRDB.SRDBControl;
import com.receteame.interfaces.ActionTypes;

import static com.receteame.interfaces.LoggerMessages.SRC_FILTRES;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
@WebServlet("/SRCFilter")
@MultipartConfig
public class SRCFiltres extends HttpServlet implements ActionTypes {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Inicialización servidor
	 */
	public void init() {
		LOGGER.info(SRC_FILTRES);
		new SRDBControl();
	}
		
	private void print (String json,HttpServletResponse response) {
		try {
			response.getWriter().print(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// NOTHING
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType(CONTENT_TYPE);
		
	    Part partFiltres = request.getPart("filtres"); 
		
		Filtres filtres = SRDBControl.parseObjectFiltres(partFiltres.getInputStream(),Filtres.class);
		
		 switch (filtres.getFiltro()) {
		    
		 case SELECT_SORT_RECEPTES_MIS_RECEPTES :
			  Part partUsuari = request.getPart("usuari");
			  Usuari usuari = SRDBControl.parseObjectUsuari(partUsuari.getInputStream(),Usuari.class);
			  Integer idUsuari = usuari.getId_usuari();
			  String jsonSelectMisReceptes = SRDBControl.getAnswerListRecepta(SRDBControl.masterQueryManagement.selectSortByUser(SELECT_SORT_RECEPTES_MIS_RECEPTES,idUsuari));
			  print(jsonSelectMisReceptes, response);
			  break;
			 
			case SELECT_SORT_RECEPTES_NOVEDADES :
				String jsonSelectNovedades = SRDBControl.getAnswerListRecepta(SRDBControl.masterQueryManagement.selectSortReceptes(SELECT_SORT_RECEPTES_NOVEDADES));
				print(jsonSelectNovedades, response);				
				break;
				
			case SELECT_SORT_RECEPTES_MIS_LIKES :
				Part partUsuariLikes = request.getPart("usuari");
				Usuari usuariLikes = SRDBControl.parseObjectUsuari(partUsuariLikes.getInputStream(),Usuari.class);
				Integer idUsuariLikes = usuariLikes.getId_usuari();
				String jsonSelectMisLikes = SRDBControl.getAnswerListRecepta(SRDBControl.masterQueryManagement.selectSortByUser(SELECT_SORT_RECEPTES_MIS_LIKES,idUsuariLikes));
				print(jsonSelectMisLikes, response);
				break;
				
			case SELECT_SORT_RECEPTES_TOP_LIKES :
				String jsonSelectTopLikes = SRDBControl.getAnswerListRecepta(SRDBControl.masterQueryManagement.selectSortReceptes(SELECT_SORT_RECEPTES_TOP_LIKES));
				print(jsonSelectTopLikes, response);
				break;
				
			case SELECT_SORT_RECEPTES_ENTRANTE :
				String jsonSelectEntrante = SRDBControl.getAnswerListRecepta(SRDBControl.masterQueryManagement.selectSortReceptes(SELECT_SORT_RECEPTES_ENTRANTE));
				print(jsonSelectEntrante, response);
				break;
				
			case SELECT_SORT_RECEPTES_SEGUNDO :
				String jsonSelectSegundo = SRDBControl.getAnswerListRecepta(SRDBControl.masterQueryManagement.selectSortReceptes(SELECT_SORT_RECEPTES_SEGUNDO));
				print(jsonSelectSegundo, response);
				break;
			
			case SELECT_SORT_RECEPTES_POSTRE :
				String jsonSelectPostre = SRDBControl.getAnswerListRecepta(SRDBControl.masterQueryManagement.selectSortReceptes(SELECT_SORT_RECEPTES_POSTRE));
				print(jsonSelectPostre, response);
				break;
		 }
		
		doGet(request, response);
	}
	
}
