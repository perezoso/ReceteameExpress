package com.receteame.servletsRecepcion;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.receteame.JavaPojos.Action;
import com.receteame.JavaPojos.Recepta;
import com.receteame.JavaPojos.Usuari;
import com.receteame.controlSRDB.SRDBControl;
import com.receteame.interfaces.ActionTypes;

import static com.receteame.interfaces.LoggerMessages.SRC_RECEPTA;

/*-***************************************************************************
 * Copyright 2016
 * © Receteame
 * Created by @author Pere Giró Guerra / Joel García Nuño.
 * Pere @mail: peregiro2323@gmail.com
 * Joel @mail: joel.garcia9510@gmail.com
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/
@WebServlet("/SRCRecepta")
@MultipartConfig(
	fileSizeThreshold = 1024 * 1024 * 10, // 20MB
	maxFileSize = 20_971_520L, // 20MB
	maxRequestSize = 41_943_040L, // 40MB
	location = "/tmp"
)
public class SRCRecepta extends HttpServlet implements ActionTypes {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Inicialización servidor
	 */
	public void init() {
		new SRDBControl();		
		LOGGER.info(SRC_RECEPTA);
	}
	
	private void print (String json,HttpServletResponse response) {
		try {
			response.getWriter().print(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// NOTHING
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType(CONTENT_TYPE);
		
		Part partAction = request.getPart("action");
		Part partRecepta = request.getPart("recepta");
		
		Action action = SRDBControl.parseObjectAction(partAction.getInputStream(),Action.class);
		Recepta recepta = SRDBControl.parseObjectRecepta(partRecepta.getInputStream(),Recepta.class);
		
		switch (action.getAction()) {
		
			case INSERT:
				String jsonInsert = SRDBControl.getAnswerRespuesta(SRDBControl.masterQueryManagement.insertRecepta(recepta));
				print(jsonInsert, response);
				break;

			case SELECT_RECEPTA:
				String jsonSelect = SRDBControl.getAnswerRecepta(SRDBControl.masterQueryManagement.selectRecepta(recepta));
				print(jsonSelect, response);
				break;
				
			case SELECT_RECEPTA_AJENA:
				Part partUsuari = request.getPart("idUsuari");
				Usuari usuari = SRDBControl.parseObjectUsuari(partUsuari.getInputStream(),Usuari.class);
				Integer idUsuari = usuari.getId_usuari();
				String jsonSelectAjena = SRDBControl.getAnswerRecepta(SRDBControl.masterQueryManagement.selectReceptaAjena(recepta,idUsuari));
				print(jsonSelectAjena, response);
				break;
				
			case UPDATE_RECEPTA:
				String jsonUpdate = SRDBControl.getAnswerRespuesta(SRDBControl.masterQueryManagement.updateRecepta(recepta));
				print(jsonUpdate, response);
				break;
				
			case DELETE_RECEPTA:
				Integer idRecepta = recepta.getId_recepta();
				String jsonDelete = SRDBControl.getAnswerRespuesta(SRDBControl.masterQueryManagement.deleteRecepta(idRecepta));
				print(jsonDelete, response);
				break;
				
			default:
				break;
				
		}
		
		doGet(request, response);
	}
	
}

