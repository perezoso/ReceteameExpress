<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>¡ Connectate a mi servicio !</title>
</head>
<body>
	<p><b>.BaseUrl</b>("https://receteame.herokuapp.com")</p>
	<p><b>@POST</b> /SRCRecepta</p>
	<p><b>@POST</b> /SRCFiltres</p>
	<p><b>@POST</b> /SRCImagen</p>
	<p><b>@POST</b> /SRCLikes</p>
	<p><b>@POST</b> /SRCSeguidor</p>
	<p><b>@POST</b> /SRCUserProfile</p>
</body>
</html>