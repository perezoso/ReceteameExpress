/*psql -h host -U usuario -W -f fichero.sql BaseDatos*/ 
/*CREACIÓN DE LA BASE DE DATOS 'PEDIDOS' */

drop database if exists receteame;

create database receteame;

\c receteame

CREATE TABLE usuaris(
id_usuari serial primary key,
nom_usuari varchar(20) NOT NULL UNIQUE,
contrasenya_usuari varchar(30) NOT NULL CHECK( length(contrasenya_usuari) > 6 ),
mail_usuari varchar(40) NOT NULL CHECK(mail_usuari ~*'.+@.+\..+') UNIQUE,
token_usuari text UNIQUE NOT NULL);

CREATE TABLE receptes(
id_recepta serial PRIMARY KEY ,
id_usuari bigint NOT NULL references usuaris(id_usuari)ON UPDATE CASCADE ON DELETE CASCADE,
titol_recepta varchar(40) NOT NULL,
numero_comensals int NOT NULL,
data_creacio_recepta date DEFAULT now(),
categoria_menu varchar(20) NOT NULL,
imatge_recepta bytea
);

CREATE TABLE ingredients(
id_ingredient int NOT NULL CHECK(id_ingredient BETWEEN 1 AND 6),
id_recepta bigint NOT NULL references receptes(id_recepta)ON UPDATE CASCADE ON DELETE CASCADE,
nom_ingredient varchar(50) NOT NULL,
quantitat double precision NOT NULL,
tipus_quantitat varchar(15) NOT NULL,
PRIMARY KEY(id_recepta, id_ingredient)
);

CREATE TABLE passos(
id_recepta bigint NOT NULL references receptes(id_recepta)ON UPDATE CASCADE ON DELETE CASCADE,
id_pas int NOT NULL CHECK(id_pas BETWEEN 1 AND 3),
descripcio_pas text NOT NULL,
PRIMARY KEY(id_recepta, id_pas),
imatge_pas bytea
);

CREATE TABLE seguidors(
id_usuari bigint NOT NULL references usuaris(id_usuari)ON UPDATE CASCADE ON DELETE CASCADE,
id_usuari_seguit bigint NOT NULL references usuaris(id_usuari)ON UPDATE CASCADE ON DELETE CASCADE,
PRIMARY KEY(id_usuari_seguit, id_usuari)
);

CREATE TABLE likes(
id_usuari bigint NOT NULL references usuaris(id_usuari)ON UPDATE CASCADE ON DELETE CASCADE,
id_recepta bigint NOT NULL references receptes(id_recepta)ON UPDATE CASCADE ON DELETE CASCADE,
PRIMARY KEY(id_recepta, id_usuari)
);



/*
create or replace function regula_like() returns trigger as $$
declare

begin
	IF (TG_OP = 'INSERT') THEN
		update receptes set contador_likes=(select contador_likes from receptes where id_recepta=new.id_recepta)+1 where id_recepta = new.id_recepta;
	ELSE
		update receptes set contador_likes=(select contador_likes from receptes where id_recepta=old.id_recepta)-1 where id_recepta = old.id_recepta;
	END IF;
return new;
end;
$$language plpgsql;

create trigger contador_likes after insert or delete on likes
for each row execute procedure regula_like();

create or replace function regula_seguidor() returns trigger as $$
declare

begin
	IF (TG_OP = 'INSERT') THEN
		update usuaris set contador_seguirdors_usuari=(select contador_seguirdors_usuari from usuaris where id_usuari=new.id_usuari_seguit)+1 where id_usuari = new.id_usuari_seguit;
	ELSE
		update usuaris set contador_seguirdors_usuari=(select contador_seguirdors_usuari from usuaris where id=old.id_usuari_seguit)-1 where id_usuari = old.id_usuari_seguit;
	END IF;
return new;
end;
$$language plpgsql;

create trigger contador_seguidors after insert or delete on seguidors
for each row execute procedure regula_seguidor();
*/

insert into usuaris (nom_usuari, contrasenya_usuari, mail_usuari, token_usuari) values ('Joel', 'rantamplan', 'asdasd@sadad.sdf', 'aaaaaaaaaa');
insert into usuaris (nom_usuari, contrasenya_usuari, mail_usuari, token_usuari) values ('Pere', 'rantamplan', 'd@sadad.sdf', 'aaaasdaaaaaaa');
insert into usuaris (nom_usuari, contrasenya_usuari, mail_usuari, token_usuari) values ('Paco', 'pacotplan', 'el@gmail.com', '12345671dqwdqwdqw');
insert into usuaris (nom_usuari, contrasenya_usuari, mail_usuari, token_usuari) values ('Sergio', 'pacotplan', 'elqweq@gmail.com', '12345671qewfqwefw');

/*
insert into receptes (id_usuari, titol_recepta, categoria_menu,numero_comensals) values (1, 'Pulpo', 'Segundo plato',3);
insert into receptes (id_usuari, titol_recepta, categoria_menu,numero_comensals) values (2, 'Macarrones', 'Entrante',1);
insert into receptes (id_usuari, titol_recepta, categoria_menu,numero_comensals) values (2, 'Crema catalana', 'Postre',1);
insert into receptes (id_usuari, titol_recepta, categoria_menu,numero_comensals) values (3, 'Lasaña', 'Segundo plato',2);
insert into receptes (id_usuari, titol_recepta, categoria_menu,numero_comensals) values (3, 'Helado casero', 'Postre',1);*/

insert into seguidors (id_usuari, id_usuari_seguit) values (1,2);
insert into seguidors (id_usuari, id_usuari_seguit) values (1,3);
insert into seguidors (id_usuari, id_usuari_seguit) values (3,2);
insert into seguidors (id_usuari, id_usuari_seguit) values (3,4);
insert into seguidors (id_usuari, id_usuari_seguit) values (2,4);
insert into seguidors (id_usuari, id_usuari_seguit) values (4,1);
insert into seguidors (id_usuari, id_usuari_seguit) values (4,2);
insert into seguidors (id_usuari, id_usuari_seguit) values (4,3);

/*insert into  likes (id_usuari, id_recepta) values (1,1);
insert into  likes (id_usuari, id_recepta) values (1,3);
insert into  likes (id_usuari, id_recepta) values (1,2);
insert into  likes (id_usuari, id_recepta) values (2,2);
insert into  likes (id_usuari, id_recepta) values (2,3);*/

/*delete from likes where id_usuari = 2 and id_recepta = 1;*/


